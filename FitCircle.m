function [X_Fit,Y_Fit,R_Fit]=FitCircle(X_bound,Y_bound,num_pnt)
% Determines boundary curve from cell boundary points with a circle with
% fourier modes.
% Uses functions 'FitFunCircle' and 'Circle_xy'.

pnt_circle = 0:(num_pnt-1);
Z = 1:length(X_bound);
L = length(Z);
phi_fit = 2*pi*(1:L)/L;

% determine center and approx. radius:
x_m = mean(X_bound);
y_m = mean(Y_bound);
Radius_v = sqrt(mean((X_bound-x_m).^2+(Y_bound-y_m).^2));
Data = [X_bound-x_m,Y_bound-y_m];

% start parameters and fit boundaries:
para0 = [Radius_v,0,0,0,0];
R_m = 1.5*max(max(abs(Data(:,1))),max(abs(Data(:,2)))); % max. radius
lim_u = [0,-R_m,-R_m,-R_m,-R_m];
lim_o = [R_m,R_m,R_m,R_m,R_m];

% fit:
option = optimset('lsqcurvefit');
optnew = optimset(option,'TolX',1e-4);
para_circle=lsqnonlin(@FitFunCircle,para0,lim_u,lim_o,option,phi_fit,Data);

% fit circle:
phi_circle = 2*pi*pnt_circle/num_pnt;
[X_Fit_0, Y_Fit_0] = Circle_xy(para_circle,phi_circle);
X_Fit = X_Fit_0 + x_m; Y_Fit = Y_Fit_0 + y_m;
R_Fit = sqrt((X_Fit-x_m).^2+(Y_Fit-y_m).^2);