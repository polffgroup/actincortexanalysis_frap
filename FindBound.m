function [X_bound,Y_bound, int_total_fr] = FindBound(im_array,sigma, sens)
% Determines boundary points of the cell according to binarized image.

% use gaussian filter:
im_array_filt = imgaussfilt(im_array,2);
% convert to binary image:
thresh = adaptthresh(im_array_filt,sens);                % threshold value
% thresh=graythresh(im_array_filt); %More robust but less precise alternative. With this option, variable sens isn't used
bin_im_array = imbinarize(im_array_filt,thresh);%,'adaptive')
se90 = strel('line', 3, 90);
se0 = strel('line', 3, 0);
bin_im_array = bwareaopen(bin_im_array,100);
bin_im_array= imdilate(bin_im_array, [se90 se0]);

% fill 'holes' in binary image:
im_filled = imfill(bin_im_array,'holes');
bin_im_array = bwareaopen(bin_im_array,12000);%The threshold pixel number should be adapted to the way cells are framed and to the image size. Typically, 12000 works well for 512*512 px images, and 2000 for 256*256 px images.
im_filled = imfill(bin_im_array,'holes');

int_total_fr=mean(mean(im_array(im_filled>0)));

% determine boundaries of all objects:`
[bounds, ~, N, ~] = bwboundaries(im_filled);    % N = number of bounds

% determine cell boundary: the boundary with biggest mean Radius:
x_M = zeros(1,N);
y_M = zeros(1,N);
R_mean = zeros(1,N);
for n = 1:N                          % loop over all boundaries
    bound = bounds{n};               % current boundary
    X_bound = bound(:,2);
    Y_bound = bound(:,1);
    
    x_M(n) = mean(X_bound);
    y_M(n) = mean(Y_bound);
    
    R = sqrt((X_bound-x_M(n)).^2+(Y_bound-y_M(n)).^2);
    R_mean(n) = mean(R);            % mean Radius of current boundary
end
[~, max_R_mean_index] = max(R_mean);
bound = bounds{max_R_mean_index};

X_bound = bound(:,2);
Y_bound = bound(:,1);