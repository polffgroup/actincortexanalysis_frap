function FitOutput = FitTimeScale2(num_tau,frametime, end_frame, intens_recovery, file_name, bleachrate)
% Applies exponential fit function to intensities of bleach region.
global fit_delay_time fit_end_time bleach_frame

FitOutput=[];

% time frame for recovery fit:
fit_delay_frames = round(fit_delay_time/frametime);
fit_end_frames = min(round(fit_end_time/frametime),end_frame-bleach_frame+1);
frameRange =(1:fit_end_frames-fit_delay_frames)';

% time scale fit:
recov_time_1 = 0;
err_recov_time_1 = 0;
amplitude_1 = 0;
err_amp_1 = 0;
if num_tau == 2
    recov_time_2 = 0;
    err_recov_time_2 = 0;
    amplitude_2 = 0;
    err_amp_2 = 0;
end
offset = 0;
err_off = 0;
rsquared = 0;
frameRange = (1:fit_end_frames-fit_delay_frames)';
%frameRange = (fit_delay_frames+1:fit_end_frames)';%alternative line to use if fit_delay_time>0

% define fit function:
switch num_tau
    case 1
        ex = @(p,t)-p(2)*exp(-t/p(1))+p(3);
        %ex = @(p,t)(-p(2)*exp(-t/p(1)));
    case 2
        ex = @(p,t)-p(2)*exp(-t/p(1))+p(5)-p(4)*exp(-t/p(3));
end

int = intens_recovery(frameRange);%intensity data

% determine start parameters:
len = length(int);
offset = mean(int(end-round(0.1*len):end));
amp = max(1,offset - int(1));
int_0 = int - offset;
eps = 0.05*(-int_0(1)); int_0_e = exp(-1) * int_0(1);
% times in interval [I/e-eps;I/e+eps]:
tau =20;

switch num_tau
    case 1
        para0 = [tau amp 1];
        % fit boundaries:
        lb= [0 0 0.5];
        ub = [10*tau 100 1.5];
    case 2
        para0 = [0.1*tau amp/2 tau amp/2 offset];
        % fit boundaries:
        lb= [0 0 0 0 0];
        ub = [10*tau 2*amp 10*tau 2*amp 2*offset];
end

try
    % Fit:
    int_min=min(int(1:3));%to skip the points where cell was not recognized
    [para,~,resid,~,~,~,jacob] = lsqcurvefit(ex,para0,frameRange(int>int_min)*frametime,smooth(int(int>int_min),3).*exp(frameRange(int>int_min)*bleachrate),lb,ub);
%	[para,~,resid,~,~,~,jacob] = lsqcurvefit(ex,para0,frameRange(int>int_min)*frametime,smooth(int(int>int_min),3),lb,ub);%Alternative: fit on data not corrected for bleaching
    % errors of fit parameters:
    del_para = nlparci(para,resid,'jacobian',jacob);
    del_para = (del_para(:,2)-del_para(:,1))/2;

    % Parameters and errors:
    ParaArray=[para; del_para']';

    recov_time_1 = para(1);
    err_recov_time_1 = del_para(1);
    amplitude_1 = para(2);
    err_amp_1 = del_para(2);
    offset = para(3);
    err_off = del_para(3);
    if num_tau == 2
        recov_time_2 = para(3);
        err_recov_time_2 = del_para(3);
        amplitude_2 = para(4);
        err_amp_2 = del_para(4);
        offset = para(5);
    err_off = del_para(5);
    end
    % Fit quality:
    mean_int = mean(int.*exp(frameRange*bleachrate));
    rsquared = 1-(sum((ex(para,frameRange*frametime)-int.*exp(frameRange*bleachrate)).^2)./sum((int.*exp(frameRange*bleachrate)-mean_int).^2));

																							 
    FitOutput=[FitOutput, ParaArray(:)', rsquared];
	
catch
    ParaArray=zeros(2,3);
    rsquared =0;
    FitOutput=[FitOutput, ParaArray(:)', rsquared];
    recov_time_1 = NaN;
    err_recov_time_1 = NaN;
    amplitude_1 = NaN;
    err_amp_1 = NaN;
    offset = NaN;
    err_off = NaN;
    rsquared = NaN;
    if num_tau == 2
        recov_time_2 = NaN;
        err_recov_time_2 = NaN;
        amplitude_2 = NaN;
        err_amp_2 = NaN;
    end
end
        
        % parameters:
        
        tau1_tot = recov_time_1;
        err_t1_tot = err_recov_time_1;
        amp1_tot = amplitude_1;
        err_amp1_tot = err_amp_1;
        off_tot = offset;
        err_off_tot = err_off;
        fit_quali_tot = rsquared;
        
        if num_tau == 2
            tau2_gau = recov_time_2;
            err_t2_gau = err_recov_time_2;
            amp2_gau = amplitude_2;
            err_amp2_gau = err_amp_2;
            tau2_erf = recov_time_2;
            err_t2_erf = err_recov_time_2;
            amp2_erf = amplitude_2;
            err_amp2_erf = err_amp_2;
            tau2_tot = recov_time_2;
            err_t2_tot = err_recov_time_2;
            amp2_tot = amplitude_2;
            err_amp2_tot = err_amp_2;
        end

    
