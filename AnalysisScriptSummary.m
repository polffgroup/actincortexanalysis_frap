function AnalysisScript()
figure(3); hold off; figure(5); hold off;
close all;
path='DirectoryPath';% directory in which the folder with measurement data are located

DirectoryLabels={'DirectoryNameA', 'DirectoryNameB', 'DirectoryNameC'};% names of the folders with measurement data that should be analysed		  
ctrl=[1 1 1 1 1];
CondLabel='DrugTreatment1'; CtrlLabel='Ctrl'; CondLabel2='DrugTreatment2'; %experimental condition labels used in file names.
fps=1; %frame rate of confocal imaging (s^-1). Works best with an integer value.
tstart=2; % time point in seconds from which the fitting starts. The first few seconds should be omitted due to cytoplasmic recovery (diffusion).
MaxTime=120; % time in seconds until which the recovery data are taken into account
intro='';
bleach_frame=6;% first frame after photobleaching.
opt=1;
if opt==2 % option that fits two time scales
    para0 = [20 0.5 50 0.5]; lb=[0 0 100 0]; ub=[100 1 2000 1];
elseif opt==1  % option that fits one time scales
    para0 = [5 0.5 1]; lb=[0 0.15 0]; ub=[50 1.05 1.05];
else % option that fits three time scales
    para0 = [20 0.5 150 0.5 0.8]; lb=[0 0 100 0 0]; ub=[100 1 2000 1 1];
end


Tensions=[];
CortFract=[];
FluorNormal=[];
CondInd=[];
Areas=[];

figure(1); hold off; colorvec=['b' 'r' 'm'];

AvgLen=0;
for k=1:length(DirectoryLabels)
    label=char(DirectoryLabels{k});
    files=dir([path   '*',label,'*']);
    dirFlags = [files.isdir];
    direc=files(dirFlags).name; direc=[direc intro];
    FRAPanalysis=importdata([path  direc '/AFMwConfocAna.txt'],'\t');
    FRAPfiles=FRAPanalysis.textdata(2:end,1);
    AvgLen=AvgLen+length(FRAPfiles);
end
RecArr_corr=NaN(AvgLen,fps*MaxTime);% fluorescence recovery with bleaching corrected
RecArr_uncorr=NaN(AvgLen,fps*MaxTime);% fluorescence recovery with bleachung uncorrected

CondInd=NaN(AvgLen,1);
taus1=NaN(AvgLen,1); taus2=NaN(AvgLen,1); mobilefrac=NaN(AvgLen,1);
QualCheck=NaN(AvgLen,1); rsquared=NaN(AvgLen,1);

preLen=0;
for k=1:length(DirectoryLabels)
    label=char(DirectoryLabels{k});
    files=dir([path   '*',label,'*']);
    dirFlags = [files.isdir];
    direc=files(dirFlags).name; direc=[direc intro];
    
    FRAPanalysis=importdata([path  direc '/AFMwConfocAna.txt'],'\t');
    FRAPdata=FRAPanalysis.data;
    FRAPfiles=FRAPanalysis.textdata(2:end,1);
    Median_bleachrate=median(FRAPdata(FRAPdata(:, 12)>0,12));
    
    for fileidx=1:length(FRAPfiles)
        fname=FRAPfiles{fileidx};
        [lsminf,~,~] = lsminfo([path direc '/' fname(1:min(strfind(fname,' '))-1) '.lsm']);
        Gain=lsminf.ScanInfo.DETECTOR_GAIN;
        LaserPower=lsminf.ScanInfo.POWER{1};
        
        
        if contains(fname, CondLabel)
            CondInd(fileidx+preLen)=1;
        elseif contains(fname, CondLabel2)
            CondInd(fileidx+preLen)=2;
        else
            CondInd(fileidx+preLen)=0;
        end
        
        try
            Aproj=FRAPdata(fileidx,1); Areas=[Areas Aproj]; %reads in the cross-sectional area of the file
            R2_BC=sqrt(Aproj/pi); Ai=0.7^2*R2_BC^2; %curvature radius R2 of the cell at the equator
        catch          
        end
        
        bleachrate=FRAPdata(fileidx, 12);
        
        try %necessary in absence of AFM data
            Tensions=[Tensions FRAPdata(fileidx, 15)];
            h=FRAPdata(fileidx, 16);
            Ncort3D=FRAPdata(fileidx, 3); Ncyto3D=FRAPdata(fileidx, 4);
            CortFract=[CortFract Ncort3D/(Ncort3D+Ncyto3D)];
            FluorNormal=[FluorNormal (Ncort3D)/(Gain*LaserPower)];
        catch
            CortFract=[CortFract -1]%to avoid subsequent error
        end
        
        try
            if isnan(RecArr_corr(fileidx+preLen,1))
                try
	                [temp1, temp2,times]=GetRecovery(char(FRAPfiles(fileidx,1))); %read in recovery curves from *.fig file
                catch
                    % if the matlab analysis failed to produce a recovery
                    % .fig file, it is possible to read in a .csv file of
                    % the recovery produced in Fiji
	                i2=min([strfind(fname,CtrlLabel)+length(CtrlLabel), strfind(fname,'bleach')+length('bleach'), strfind(fname,'Bleach')+length('Bleach'),strfind(fname,CondLabel)+length(CondLabel),strfind(fname,CondLabel2)+length(CondLabel2)]);
                    if length(dir([path   direc '/' fname(1:i2-1) '*.csv'])); % fluorescence recovery curve can also be read in from csv file generated with fiji
                        CSVfile=dir([path  direc '/'  fname(1:i2-1) '*.csv']);
	                    temp1 = readtable([path  direc '/'  CSVfile(1).name]);
	                    times=temp1(:,1).Variables; times=times(bleach_frame+1:end);
	                    temp1=temp1(:,2).Variables;
	                    temp1=temp1(bleach_frame+1:end)/mean(temp1(1:bleach_frame)); temp2=temp1.*exp((1:length(temp1))/Median_bleachrate)';
	                end
	             end
                %temp1 and temp2 get the intensity curves over time with
                %bleaching uncorrected (temp2) and bleaching corrected (temp1)
                delt=times(2)-times(1);
                if sum((temp1==0))>0
                end
                indices=(temp1>=min(temp1(1:3)));%remove frames where cell boundary was not properly recognized
                if length(strfind(fname, CondLabel))>0
	                end
                
                %temp1=temp2;%Alternative: uncomment to perform the analysis without bleaching correction
				
                %Interpolation of intensity values to definite time points. The first time point is 0.
                try
                    RecArr_corr(fileidx+preLen,:)=[smooth(interp1(times(indices)-times(1), temp1(indices),(0:(fps*min(MaxTime-1/fps,floor(times(end)-times(1)))))/fps),1)', temp1(end)*ones(1,fps*(MaxTime-floor(times(end)-times(1))-1/fps))];%If the acquisition time was shorter than MaxTime, the last measured value is repeated.
                    if bleachrate<0 % reject bleaching corrections with a negative bleachrate and correct bleaching instead  with the median bleachrate from the data set
                        RecArr_corr(fileidx+preLen,:)=[smooth(interp1(times(indices)-times(1), temp2(indices).*exp(times(indices)/(delt*Median_bleachrate)),(0:(fps*min(MaxTime-1/fps,floor(times(end)-times(1)))))/fps),1)', temp2(end)*exp(times(end)/(delt*Median_bleachrate))*ones(1,fps*(MaxTime-floor(times(end)-times(1))-1/fps))];
                    end
                catch
                    waitforbuttonpress;%for issues with non-integer fps values
                end
                RecArr_uncorr(fileidx+preLen,:)=[smooth(interp1(times(indices)-times(1), temp2(indices), (0:(fps*min(MaxTime-1/fps,floor(times(end)-times(1)))))/fps),1)', temp2(end)*ones(1,fps*(MaxTime-floor(times(end)-times(1))-1/fps))];
                if floor(times(end)-times(1))<MaxTime-1/fps
                    RecArr_uncorr(fileidx+preLen,(floor(fps*(times(end)-times(1)))+2):MaxTime*fps)=NaN;
                    RecArr_corr(fileidx+preLen,(floor(fps*(times(end)-times(1)))+2):MaxTime*fps)=NaN;%Replaces the repeated last measured value with NaNs, if needed. Then must use 'omitnan' when averaging.
                end
                
            end
            
            if opt==2 %fit two time scales
                ex = @(p,t)(1-p(2)*exp(-t/p(1))-(1-p(2))*exp(-t/p(3)))*(1-p(4))+p(4);
            elseif opt==1 % fit one time scale
                ex = @(p,t) -exp(-t/p(1))*p(2)+p(3);
            else % fit three time scales (not recommended)
                ex = @(p,t)(1-p(2)*exp(-t/p(1))-(1-p(2))*exp(-t/p(3)))*(1-p(4))*p(5)+p(4);
            end
            
            %fitting of exponential recovery curve:
            delt=times(2)-times(1); % time interval between frames
            xvec=((fps*tstart):(fps*min(MaxTime-1/fps,floor(times(end)-times(1)))))/fps;
            yvec=RecArr_corr(fileidx+preLen,(fps*tstart+1):(fps*min(MaxTime,floor(times(end)-times(1)+1/fps))));
            [para1,~,resid,~,~,~,jacob] = lsqcurvefit(ex,para0,xvec(isfinite(yvec)),yvec(isfinite(yvec)),lb, ub);
            rsquared(fileidx+preLen)= 1-(sum((ex(para1, xvec)-yvec).^2)./sum((yvec-mean(yvec)).^2));
                       
            %Plot normalized recovery curves 
            if opt>1 %two time scales
                RecArr_corr(fileidx+preLen,:)= (RecArr_corr(fileidx+preLen,:)-para1(4))/(1-para1(4));
                taus1(fileidx+preLen)=para1(1); taus2(fileidx+preLen)=para1(3);
                mobilefrac(fileidx+preLen)=para1(2);
            else % one time scale
                taus1(fileidx+preLen)=para1(1); taus2(fileidx+preLen)=para1(2);%disp(para1(1));
                mobilefrac(fileidx+preLen)=para1(2)/(1-para1(3)+para1(2));
            end
            
            figure(1);
            plot((1:fps*MaxTime)/fps, RecArr_corr(fileidx+preLen,:),[colorvec(CondInd(fileidx+preLen)+1) 'o']); hold on ;
			title([direc fname],'Interpreter','none');
        catch
            1
        end
                
        
    end
    preLen=preLen+length(FRAPfiles);
end

close all;

figure(2);
subplot(1,5,1:2.5);
hold off;


titlestring=[];
if sum(CondInd==2)>0
    indend=3;
else
    indend=2;
end

qcc=0.0; % condition on quality check value
for i=1:indend
    %plot averaged, normalized recovery curves plus and minus standard
    %error of the mean
    plot((1:fps*MaxTime-1)/fps,mean(RecArr_corr(CondInd==(i-1) & rsquared>qcc,2:end),'omitnan')+std(RecArr_corr(CondInd==(i-1) & rsquared>qcc,2:end),'omitnan')/sqrt(sum(CondInd==(i-1),'omitnan')),'Color',colorvec(i));
    hold on ;
    plot((1:fps*MaxTime-1)/fps,mean(RecArr_corr(CondInd==(i-1) & rsquared>qcc,2:end),'omitnan')-std(RecArr_corr(CondInd==(i-1) & rsquared>qcc,2:end),'omitnan')/sqrt(sum(CondInd==(i-1),'omitnan')),'Color',colorvec(i));
    plot((1:fps*MaxTime-1)/fps,mean(RecArr_corr(CondInd==(i-1) & rsquared>qcc,2:end),'omitnan'),[colorvec(i) 'o-']);
    
	%Fit the averaged recovery curve:
	xvec=(fps*tstart:(fps*MaxTime-1))/fps;
    yvec=mean(RecArr_corr(CondInd==(i-1)  & rsquared>qcc,((fps*tstart+1):fps*MaxTime)));
    try
        [para1,~,resid,~,~,~,jacob] = lsqcurvefit(ex,para0, xvec(isfinite(yvec)),yvec(isfinite(yvec)),lb, ub);
    catch
    end
    % errors of fit parameters:
    del_para1 = nlparci(para1,resid,'jacobian',jacob);
    del_para1 = (del_para1(:,2)-del_para1(:,1))/2;
    plot(xvec(isfinite(yvec)),ex(para1, xvec(isfinite(yvec))),'g--','LineWidth',2)%Fitting line plotted up to the end of the fitting range
    titlestring=[titlestring num2str([para1, sum(CondInd==(i-1) & (rsquared>qcc),'omitnan')]), '  \n'];
end
title(sprintf(['Averaged Recoveries \n' titlestring]));
ylim([0, 1]);


try %to avoid error in absence of AFM data
    subplot(1,5,3); % plot boxplots of cortical tension in different conditions
    hold off; len=zeros(1,3);
    for i=1:indend; len(i)=sum(CondInd==(i-1)  & (rsquared>qcc)); end;
    TensArrays=NaN(indend,max(len));
    for i=1:indend; TensArrays(i,1:len(i))=Tensions(CondInd==(i-1) & (rsquared>qcc)); end; % the correction factor of 0.817 should be factored beforehand, in AnalyseFRAPcurves using GetTension
    if indend==3
        labels={CtrlLabel, CondLabel, CondLabel2};
    elseif indend==2
        labels={CtrlLabel, CondLabel};
    else
        labels={CtrlLabel};
    end
    boxplot(TensArrays', 'Labels', labels); clear('labels');
    xlim([0.5 indend+0.5]); ylim([0, max(Tensions(:))])
    try
        title(sprintf(['Cortical tension \n' num2str(len) num2str(ranksum(TensArrays(1,:),TensArrays(2,:)))]))% ranksum(TensArrays(1,:),TensArrays(3,:)) ranksum(TensArrays(2,:),TensArrays(3,:))]);
    catch
        title(sprintf('Cortical tension'))
    end
    titlestring=[]; for k=1:length(DirectoryLabels); titlestring=[titlestring  char(DirectoryLabels{k})];end
catch
end
    
subplot(1,5,4); % plot boxplots of recovery times
hold off;
for i=1:indend;
    len(i)=sum(CondInd==(i-1) & rsquared>qcc); end;
TauArrays=NaN(indend,max(len));
for i=1:indend;
    TauArrays(i,1:len(i))=taus1(CondInd==(i-1) & rsquared>qcc); end;
boxplot(TauArrays');ylim([0,1.1*max(taus1)])
xlim([0.5 indend+0.5])
try
    title(sprintf( ['Recovery times \n' num2str([len ranksum(TauArrays(1,:),TauArrays(2,:))])...
    '\n' num2str([median(TauArrays(1,:),'omitnan') median(TauArrays(2,:),'omitnan')])]))% ranksum(TauArrays(1,:),TauArrays(3,:)) ranksum(TauArrays(2,:),TauArrays(3,:))]);
catch
    title('Recovery times')
end

subplot(1,5,5); % plot boxplots of cortical fraction
hold off;
for i=1:indend;
    len(i)=sum(CondInd==(i-1) & (rsquared>qcc));
end;
CortFractArrays=NaN(indend,max(len));
for i=1:indend;
    CortFractArrays(i,1:len(i))=CortFract(CondInd==(i-1) & (rsquared>qcc));
end;
boxplot(CortFractArrays');
try
    xlim([0.5 indend+0.5]); ylim([0, 1.1*max(CortFract(:))])
catch
    xlim([0.5 indend+0.5]); ylim([0, 1])%In case there's no info on recovering fractions
end
try
    title(sprintf( ['Cortical Fraction \n' num2str([len ranksum(CortFractArrays(1,:),CortFractArrays(2,:))]) ...
   '\n' num2str([median(CortFractArrays(1,:),'omitnan') median(CortFractArrays(2,:),'omitnan')])]))% ranksum(CortFractArrays(1,:),CortFractArrays(3,:)) ranksum(CortFractArrays(2,:),CortFractArrays(3,:))]);
catch
    title('Cortical Fraction')
end


titlestring=[];
for k=1:length(DirectoryLabels); titlestring=[titlestring  char(DirectoryLabels{k})]; end;
saveas(gcf,[path,'AveragedRecovery' titlestring,'_',num2str(opt), 'B.fig'])
%saveas(gcf,[path,'GatheredDatab' titlestring, 'B.fig'])

%figure; plot(FluorNormal(CondInd==0), mobilefrac(CondInd==0),'ro')

[p, h]=corr([TensArrays(1,isfinite(TensArrays(1,:))) TensArrays(2,isfinite(TensArrays(2,:)))]',...
    [TauArrays(1,isfinite(TensArrays(1,:))) TauArrays(2,isfinite(TensArrays(2,:)))]')

%plot recovery times versus cortical tension values
if indend==3
    temp1=[TensArrays(1,isfinite(TensArrays(1,:))) TensArrays(2,isfinite(TensArrays(2,:))) TensArrays(3,isfinite(TensArrays(3,:)))];
    temp1=[ones(length(temp1),1) temp1'];
    a=temp1\[TauArrays(1,isfinite(TensArrays(1,:))) TauArrays(2,isfinite(TensArrays(2,:))) TauArrays(3,isfinite(TauArrays(3,:)))]';
    figure(4); hold off; plot(TensArrays(1,isfinite(TensArrays(1,:))),TauArrays(1,isfinite(TensArrays(1,:))),'o')%changed to include all points
    hold on; plot(TensArrays(2,isfinite(TensArrays(2,:))),TauArrays(2,isfinite(TensArrays(2,:))),'ro')
    hold on; plot(TensArrays(3,isfinite(TensArrays(3,:))),TauArrays(3,isfinite(TensArrays(3,:))),[colorvec(3) 'o'])%added to plot cond2
elseif indend==2
    temp1=[TensArrays(1,isfinite(TensArrays(1,:))) TensArrays(2,isfinite(TensArrays(2,:)))];
    temp1=[ones(length(temp1),1) temp1'];
    a=temp1\[TauArrays(1,isfinite(TensArrays(1,:))) TauArrays(2,isfinite(TensArrays(2,:)))]';
    figure(4); hold off; plot(TensArrays(1,isfinite(TensArrays(1,:))),TauArrays(1,isfinite(TensArrays(1,:))),'o')
    hold on; plot(TensArrays(2,isfinite(TensArrays(2,:))),TauArrays(2,isfinite(TensArrays(2,:))),'ro')
else
    temp1=[TensArrays(1,isfinite(TensArrays(1,:)))]
    temp1=[ones(length(temp1),1) temp1'];
    a=temp1\[TauArrays(1,isfinite(TensArrays(1,:)))]';
    figure(4); hold off; plot(TensArrays(1,isfinite(TensArrays(1,:))),TauArrays(1,isfinite(TensArrays(1,:))),'o')
end
hold on ; plot(0:4, a(1)+a(2)*(0:4))
title([num2str(a') ' ' num2str(p) ' ' num2str(h)])
saveas(gcf,[path,'TausVsTension' titlestring '.fig'])

%Quick plot of mobile fractions:
MobFracArrays=NaN(indend,max(len));
for i=1:indend
    MobFracArrays(i,1:len(i))=mobilefrac(CondInd==(i-1) & rsquared>qcc); 
end
figure;boxplot(MobFracArrays');
title(sprintf( ['Mobile Fraction \n' num2str([len ranksum(MobFracArrays(1,:),MobFracArrays(2,:))]) ...
    '\n' num2str([median(MobFracArrays(1,:),'omitnan') median(MobFracArrays(2,:),'omitnan')])]))
1
    
    
    function [int1, int2, times]=GetRecovery(FRAPfile)%,indicl, RecArr_corr, RecArr_uncorr)
        hold off;
        if length(strfind(intro,'FRAP'))>1
            fig=openfig([path, direc, '/',FRAPfile(1:((strfind(FRAPfile,' '))-1)),'FRAP_recovery.fig']);
        elseif length(intro)>1
            fig=openfig([path, direc,'/OldAna', intro, '/',FRAPfile(1:((strfind(FRAPfile,' '))-1)),'_recovery.fig']);
        else
            fig=openfig([path, direc, '/',FRAPfile(1:((strfind(FRAPfile,' '))-1)),'_recovery.fig']);
        end
        axesObjs = get(fig, 'Children');
        dataObjs = findall(fig, '-property', 'xdata'); %handles to low-level graphics objects in axes
        xdata = get(dataObjs, 'XData');  %data from low-level grahics objects
        ydata = get(dataObjs, 'YData');
        times=xdata{1};
        recovery_corr=ydata{2};%(ydata{4}-ydata{1}(1))/(1-ydata{1}(1));
        recovery_uncorr=ydata{1};%(ydata{3}-ydata{1}(1))/(1-ydata{1}(1));
        int1=recovery_corr;
        int2=recovery_uncorr;
    end
end





