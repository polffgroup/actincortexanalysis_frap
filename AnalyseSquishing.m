% Program input for each cell:
%   - an LSM (Zeiss confocal microscope) file showing time series of a
%     cross-section of a cell fluorescently labeled boundary
%   - a TIFF file generated from the LSM file using Fiji (see below)
%   - (optionnally) an AFM readout file with the measured force and
%     cantilever height over time (JPK output format *.out).
%
% Program output for each cell:
%   - a time series of the cortical intensity ratio values and,
%     potentially, the cortical tension values during confocal imaging,
%     [FileName "_Tension_OuterOverInnerRingFluo5a.fig"] or
%     [FileName "_OuterOverInnerRingFluo5a.fig"]
%   - a time series of the cortical tension and cell confinement height
%     values [FileName "_Tension.fig"]
%   - a kymograph of cortical fluorescence intensity over time
%     [FileName "_int_kymograph.fig"]
%   - a MAT file containing all the variables created in this script
%     [FileName "d.mat"].
%
% Program uses the sub-routines: FindBound, FitSmooth, GetTension2,
% CompCellRotation, FrameIntensities2 
%
% To generate the TIFF file, the LSM file must be opened in Fiji and saved
% as a TIFF file after the following modifications of the image series:
%   For "cell squishing" experiments, the TIFF time series of images must
%   start with the first frame acquired after the cantilever reaches its
%   final, low position.
%   For "blue light photoactivation" experiments, the TIFF time series
%   must start with the very first acquired frame.
%   In both cases, it can be beneficial to crop the images into a smaller
%   square containing the cell.
%
% This script must be run twice.
% First pass generates the"_Tension.fig" and "d.mat" files.
% Second pass generates the kymograph and
% "_Tension_OuterOverInnerRingFluo5a.fig" file.

function AnalysisSquishing()
global offset Kcant cantspike CondLabel CtrlLabel sens isAFM

%Parameters that should be set
path='yourpathname';%path to the folder where the input files are stored
sens=.65; %sensitivity parameters that is used in the determination of the cell boundary, see function Findbound.m
isAFM=1; %boolean, needs to be set to 0 if no AFM data is available
offset=-1; cantspike=0.3;  Kcant=428.8;% offset: z-coordinate of dish bottom in AFM readout, cantspike: size of cantilever spike on wedge, Kcant: measured cantilever stiffness in measurement (without the 0.817 correction for rectangular cantilevers). Distances in um, cantilever stiffness in nN/um.
pattern='Cell*';%string at the beginning of all file names

FTavg=zeros(90,10); counter=0;

path0=path; 
list_s = dir([path pattern '.lsm']); end_frame2=0; h2=0;
list_c = struct2cell(list_s);
num_files = size(list_c,2);
mycolors=hsv(ceil(max([num_files 15])));

h = gcf;
axesObjs = get(h, 'Children');
dataObjs = findall(h, '-property', 'xdata'); %handles to low-level graphics objects in axes
xdata2 = get(dataObjs, 'XData');  %data from low-level grahics objects
ydata2 = get(dataObjs, 'YData');
close all;

for file_num =1:length(list_c(1,:))
    list_s = dir([path0 pattern '.lsm']);
    list_c = struct2cell(list_s);
    num_files = size(list_c,2);
    fname = list_c{1,file_num};
    fname2 = [fname(1:end-4) '_continued','.lsm']; %for processing of image acquisition split into two LSM files
    
    if length(strfind(fname, 'contin'))>0
        break;
    end

    bleachrate=1/4000; ctrl=1;
    
    tiffile = dir([path0 fname(1:10),'*.tif']);
    tiffile2 = dir([path0 fname2(1:end-3),'tif']);
    Datafile=dir([path0 '*' fname(1:end-4) '*d.mat']);
    
    try
    
    if length(Datafile)==0 %First pass:
        num_linep = 200;        % number of points on orthog. lines along cortex
        num_fitp = 500;         % points for boundary fit
        sigma=1; bleach_frame=1000;
        
        % get video properties:
        [lsminf,~,~] = lsminfo([path0 fname(1:end-4),'.lsm']);
        info = imfinfo([path0 tiffile.name]);
        num_frames =numel(info);
        
        try
            [lsminf2,~,~] = lsminfo([path0 fname2(1:end-4),'.lsm']);
            num_frames2 = lsminf2.TIMESTACKSIZE;
            delt2=median(lsminf2.TimeStamps.TimeSteps);
        catch
            num_frames2 =0;delt2=1;
        end
        
        timesteps = lsminf.TimeStamps.TimeSteps;
        frametime = median(timesteps); % time per frame
        delt=frametime;
        px = lsminf.VoxelSizeX * 10.^6; % pixel size in um
        len_lines = round(6/px); % Length of orthogonal lines along cortex in pixels. Set to correspond to 6 um.
        
        
        info=imfinfo([path0 fname]);
        try
            end_frame=lsminf.TIMESTACKSIZE+lsminf2.TIMESTACKSIZE;
        catch
            end_frame=lsminf.TIMESTACKSIZE;
        end
        
        %Initialization of variables for this cell:
        Rcells= zeros(1,end_frame);
        
        bounds_all = cell(end_frame,2); fit_all = cell(end_frame,3);
        intens_all = zeros(end_frame,num_fitp);fit_bleach_all = zeros(end_frame,num_fitp);
        int_cort_mean_data_all = zeros(end_frame,num_linep); intens_inner_all = zeros(end_frame,1);intens_outer_all = zeros(end_frame,1);
        int_total=zeros(end_frame,1);
%         int_cort_data_all=zeros(end_frame,num_linep,num_fitp); %Optional variable: can be used in case a region of the cortex needs to be excluded from the analysis. Uncomment lines: 122, 180, 221, and 390. And set cort_lb and cort_ub according to the postion of the region to exclude.
%         cort_lb=30; cort_ub=60; %Optional variable: see line 121. Set so as to flank the position of the cortex region to exclude.

        index_rotation = 0;
        
        area_cross_inner_all = zeros(end_frame,1);area_ring_all = zeros(end_frame,1);
        vol_inner_all = zeros(end_frame,1);vol_cort_all = zeros(end_frame,1);
        peri_cell_all = zeros(end_frame,1); area_cell_all = zeros(end_frame,1); vol_cell_all = zeros(end_frame,1);
        
        ind_bl_reg_mid = 0; reg_lb_ind = 0; reg_ub_ind = 0; im_save = cell(end_frame);
        
        % Set arrays with data for all frames:
        DataAll = {bounds_all,fit_all,intens_all,fit_bleach_all,int_cort_mean_data_all,intens_inner_all, int_total};
        % Cell dimensions
        AreaVols = {area_cross_inner_all,area_ring_all, vol_inner_all,vol_cort_all,peri_cell_all, area_cell_all,vol_cell_all, int_total};
        
        %----------
        
        for fnum = 1:num_frames %Go through the image time series to fit the cortex and collect the intensity values at every frame
            if length(tiffile)>0
                try
                    im_array = imread([path0, char(tiffile(1).name)],fnum);
                catch
                end
            else
                im_array_0 = imread([path0, fname],2*fnum-1);
                im_array = im_array_0(:,:,1);
            end

            figure(1); hold off; imagesc(im_array); hold on
            [X_bound,Y_bound] = FindBound(im_array,sigma,sens);
            bounds_all{fnum,1} = X_bound;
            bounds_all{fnum,2} = Y_bound;
            [X_Fit,Y_Fit,R_Fit]=FitSmooth(X_bound,Y_bound, num_fitp);
            bound_fit_all{fnum,1} = X_Fit;
            bound_fit_all{fnum,2} = Y_Fit;
            bound_fit_all{fnum,3} = R_Fit;
            
            % Compensate cell rotation:
            [bound_fit_all,~] = CompCellRotation(bound_fit_all,fnum);
            X_Fit = bound_fit_all{fnum,1};
            Y_Fit = bound_fit_all{fnum,2};
            %Rapprox=mean(R_Fit);
            % X_Fit=(1-0.5/Rapprox)*(X_Fit-Rapprox)+Rapprox;
            % Y_Fit=(1-0.5/Rapprox)*(Y_Fit-Rapprox)+Rapprox;
            meanx=mean(X_Fit);
            meany=mean(Y_Fit);
            
            [intens_frame,int_cort_mean_data,int_cort_data, AreaVol,intens_inner_fr_mean, intens_outer_fr_mean] = FrameIntensities2(im_array,X_Fit,Y_Fit,num_linep,fnum,px,...
                len_lines,bleach_frame);

            temp = ['fnum=' num2str(fnum)];
            text(10,10,temp,'Color',[1 1 1]);
            
            intens_all(fnum,:) = intens_frame;
            int_cort_mean_data_all(fnum,:) = int_cort_mean_data;
            int_total(fnum,:)=mean(im_array(:));
            intens_inner_all(fnum)=intens_inner_fr_mean;
            intens_outer_all(fnum)=intens_outer_fr_mean;
%             int_cort_data_all(fnum,:,:)=int_cort_data; %Optional. See line 117.
            
            area_cross_inner_all(fnum) = AreaVol{1};
            area_ring_all(fnum) = AreaVol{2};
            vol_inner_all(fnum) = AreaVol{3};
            vol_cort_all(fnum) = AreaVol{4};
            peri_cell_all(fnum) = AreaVol{5};
            area_cell_all(fnum) = AreaVol{6};
            vol_cell_all(fnum) = AreaVol{7};
            
            
        end
        try
            for fnum = (num_frames+1):end_frame
                if length(tiffile2)>0
                    im_array = imread([path0, char(tiffile2(1).name)],fnum);
                else
                    im_array_0 = imread([path0, fname2],2*(fnum-num_frames)-1);
                    im_array = im_array_0(:,:,1);
                end
                
                figure(1); hold off; imagesc(im_array); hold on
                [X_bound,Y_bound] = FindBound(im_array,sigma);
                bounds_all{fnum,1} = X_bound;
                bounds_all{fnum,2} = Y_bound;
                [X_Fit,Y_Fit,R_Fit]=FitSmooth(X_bound,Y_bound, num_fitp);
                
                bound_fit_all{fnum,1} = X_Fit;
                bound_fit_all{fnum,2} = Y_Fit;
                bound_fit_all{fnum,3} = R_Fit;
                
                [intens_frame,int_cort_mean_data,int_cort_data, AreaVol,intens_inner_fr_mean, intens_outer_fr_mean] = FrameIntensities2(im_array,X_Fit,Y_Fit,num_linep,fnum,px,...
                    len_lines,bleach_frame);
                txt = ['fnum=' num2str(fnum)];
                text(10,10,txt,'Color',[1 1 1]);
                
                intens_all(fnum,:) = intens_frame;
                int_cort_mean_data_all(fnum,:) = int_cort_mean_data;
                int_total(fnum,:)=mean(im_array(:));
                intens_inner_all(fnum)=intens_inner_fr_mean;
                intens_outer_all(fnum)=intens_outer_fr_mean;
%                 int_cort_data_all(fnum,:,:)=int_cort_data;%Optional. See line 117.
                
                area_cross_inner_all(fnum) = AreaVol{1};
                area_ring_all(fnum) = AreaVol{2};
                vol_inner_all(fnum) = AreaVol{3};
                vol_cort_all(fnum) = AreaVol{4};
                peri_cell_all(fnum) = AreaVol{5};
                area_cell_all(fnum) = AreaVol{6};
                vol_cell_all(fnum) = AreaVol{7};
            end
        catch
        end

        
        % cortex and cytosol intensities:
        cytosol_3D=zeros(1,end_frame);
        cytosol_2D=zeros(1,end_frame);
        cortex_2D = zeros(1,end_frame);
        cortex_3D = zeros(1,end_frame);
        cortcyt_2D = zeros(1,end_frame);
        cortcyt_3D =zeros(1,end_frame);
        Paras=zeros(end_frame,6);
        
        for i=1:end_frame %fit the cortex radial intensity profile and plot it for every frame
            int_cort = int_cort_mean_data_all(i,:);
            
            s=linspace(0,1,num_linep);
            [int_cort_fit,para_cortex,int_cort_fkt, int2_cort_fkt, skewgau,err] = FitCortexIntensity(int_cort);
            int_cort_fit_cyto = mean(int_cort(1:5));
            intens_inner_mean = intens_inner_all(i);
            Paras(i,:)=para_cortex;
            
            % cell areas and volume from time span before bleaching
            area_cross_inner_mean = area_cross_inner_all(i);
            area_ring_mean = area_ring_all(i);
            crosssect_area=area_ring_mean+area_cross_inner_mean;
            R2_BC=sqrt(crosssect_area/pi);
            
            RCells(i)=sqrt((area_cross_inner_mean+area_ring_mean)/pi);
            if i==1
                try
                    [h,TensVal]=GetTension2(path, Kcant, cantspike, fname, crosssect_area);%call to the function in file GetTension2.m, which processes the AFM .out file and creates the "_Tension.fig" file
                catch
                    h=0; TensVal=0;
                end
                area_cell_mean = Area(R2_BC', h/2, h); % total cell area
                
                vol_inner_mean = Volume(sqrt(area_cross_inner_mean/pi), h, h/2);
                vol_cell_mean = Volume(R2_BC', h, h/2);
                vol_cort_mean = vol_cell_mean-vol_inner_mean;
                peri_cell_mean =peri_cell_all(i);
             end
            
            
            % Cytosol intensities from time span before bleaching
            % crosssection
            cyto_inner_2D = area_cross_inner_mean * (intens_inner_mean-para_cortex(5));
            cyto_ring_2D = area_ring_mean * (int_cort_fit_cyto);
            cytosol_2D(i) = cyto_inner_2D + cyto_ring_2D;
            
            % 3D cell from time span before bleaching
            cyto_inner_3D = vol_inner_mean * (intens_inner_mean-para_cortex(5));
            cyto_ring_3D = vol_cort_mean * (int_cort_fit_cyto-para_cortex(5));
            cytosol_3D(i) = cyto_inner_3D + cyto_ring_3D;
            
            % Cortex from time span before bleaching
            integr_gau = trapz(s,skewgau(para_cortex,s))*px*len_lines;
            cortex_2D(i) = peri_cell_mean * integr_gau; % crosssection
            cortex_3D(i) = area_cell_mean * integr_gau; % 3D cell
            
            figure(2); hold off;
            plot(s*len_lines*px,int_cort); hold on
            plot(s*len_lines*px,int2_cort_fkt(para_cortex,s));
            legend('mean data','Fit'); set(gca,'YLim',[0,180]);
            title(['Averaged radial Intensity profile of cell cortex, mean for frames 1-',...
                num2str(bleach_frame),' and whole cell perimeter']);
            if i==200
            end
        end
        close(figure(2))
        
        save([path fname(1:end-4) 'd.mat'],'-v7.3');%save all the variables created in the first pass
        

    else %Second pass:
        load([path0 Datafile(1).name]);
        try
            Fitcortex(1); %fit the cortex radial intensity profile at frame. Used for background intensity correction with "para_cortex(5)"
            if length(strfind(fname, '_Squish'))>0 %"cell squishing" experiment
                if length(dir([path0 '*' fname(1:end-4) '*OuterOverInnerRingFluo4a.fig']))==0
                PlotAnalysis2();
%                 FTrans=FourierAnalys();%optional: Fourier transform analysis of the fluorescence intensity around the cortex
%                 FTavg=FTavg+FTrans(1:90,2:11); counter=counter+1;
                end
                
            else %"blue light photoactivation" experiment
                if length(dir([path0 '*' fname(1:end-4) '*OuterOverInnerRingFluo4a.fig']))==0
                PlotAnalysis();
%                 FTrans=FourierAnalys();%optional: Fourier transform analysis of the fluorescence intensity around the cortex
%                 FTavg=FTavg+FTrans(1:90,2:11); counter=counter+1;
                end
            end
        catch ME1
            disp(ME1)
        end
    end
    catch ME2
        disp(ME2)
    end
end
%figure; contourf(movmean(FTavg,1))
%figure; plot(smooth(FTavg(:,1)))
1

    function PlotAnalysis()%for "blue light photoactivation" experiments
        try
            times=[lsminf.TimeStamps.TimeStamps; lsminf2.TimeStamps.TimeStamps+lsminf.TimeStamps.TimeStamps(end)];
        catch
            times=lsminf.TimeStamps.TimeStamps;
        end
        try
            BlueLaserTime=find((diff(times)>(1.01*delt)) | (diff(times)<(0.99*delt)));
            BlueLaserTime=BlueLaserTime(1);%index of the frame acquired right before the activating blue light scan.
        catch
            BlueLaserTime=5;
        end
        times=times-times(BlueLaserTime(1));%origin of times set at the beginning of the blue light scan
        
        
        if isAFM %generate the time series of cortical tension, from the "_Tension.fig" file
            TensionFile=dir([path0 '*' fname(1:6)  '*_Tension.fig']); TensionFile=TensionFile(1).name;
            fig=openfig([path0, TensionFile]);
            axesObjs = get(fig, 'Children');
            dataObjs = findall(fig, '-property', 'xdata'); %handles to low-level graphics objects in axes
            xdata = get(dataObjs, 'XData');  %data from low-level grahics objects
            ydata = get(dataObjs, 'YData');
            i=find(cellfun(@length, ydata)>2 & cellfun(@length, ydata)<4500,2);
            i=i(2); ctrl=1;
            Tension=ydata{i}; %correction factor of 0.817 already applied in GetTension2.m
            TensTimes=xdata{i}; deltTens=TensTimes(2)-TensTimes(1);
            TensIncWin=find(smooth(diff(Tension(max(100,length(Tension)-2200):end-200)),10)>0.01);
            TensIncWin=TensIncWin(diff(TensIncWin)==1);%ranges of time points when the the tension increases significantly
            indp=min(TensIncWin)+max(100,length(Tension)-2200)+2-round(delt*BlueLaserTime(1)/deltTens);%index of the estimated time point when photoactivation started
            TensionTimeWindow=deltTens*(0:round((times(end)+delt*BlueLaserTime(1))/deltTens))-delt*BlueLaserTime(1);
            TensionWindow=Tension(indp:(indp+round((times(end)+delt*BlueLaserTime(1))/deltTens)));  
        end
        
        fac=1;%Weight of the background correction. Typically set to 1
        close all
        hold off;
        
        temp=(mean(int_cort_mean_data_all(1:length(times),40:120),2)-fac*para_cortex(5))./(intens_inner_all(1:length(times))-fac*para_cortex(5));%cortical intensity ratio, with background correction
        
        %temp=(mean(mean(int_cort_data_all(1:length(times),40:120,[1:cort_lb,cort_ub:num_fitp]),3),2)-fac*para_cortex(5))./(intens_inner_all(1:length(times))-fac*para_cortex(5)); %Alternative. See line 117 and set cort_lb and cort_ub accordingly.
        %temp=(mean(int_cort_mean_data_all(1:length(times),40:120),2)-fac*para_cortex(5));%Alternative: absolute cortex intensity, with background correction
        %temp=(intens_inner_all(1:length(times))-fac*para_cortex(5));%Alternative: absolute cytoplasm intensity, with background correction
        
        if isAFM
            [AX,H1,H2] = plotyy(smooth(times, 10), smooth(temp,10), TensionTimeWindow,TensionWindow);
            set(AX(1),'NextPlot','add');
            hold on ; plot(AX(1),times, temp,'bo');
            set(get(AX(1),'Ylabel'),'String','Cortical fluorescence (a.u.)');hold on;
            set(get(AX(2),'Ylabel'),'String','Cortical Tension (mN/m)');hold on;
            set(H1,'LineWidth',2)
            set(H2,'LineWidth',2)
            title(fname)
            saveas(gcf, [path fname(1:end-4) 'Tension_OuterOverInnerRingFluo5a.fig'])
        else
            H1 = plot(smooth(times, 10), smooth(temp,10));hold on;
            plot(times, temp,'bo');
            ylabel('Cortical fluorescence (a.u.)');
            set(H1,'LineWidth',2)
            title(fname)
            saveas(gcf, [path fname(1:end-4) '_OuterOverInnerRingFluo5a.fig'])
        end
        
        close(gcf);
%         save([path0 fname '_intRatios.mat'], 'temp');%Optional: to directly save the intensity ratios in a data file.
        
        figure(6); hold off;
        PlotIntensities(intens_all,figure(1),1, bleachrate);%generate the kymograph of cortex intensities
        savefig([path0 fname,'_int_kymograph.fig']);
    end


    function PlotAnalysis2()%for "cell squishing" experiments
        info = imfinfo([path0 tiffile.name]);
        TimeCut=end_frame-numel(info); TimeCut=2;
        try
            times=[lsminf.TimeStamps.TimeStamps; lsminf2.TimeStamps.TimeStamps+lsminf.TimeStamps.TimeStamps(end)];
        catch
            times=lsminf.TimeStamps.TimeStamps;
        end
        times=times(TimeCut+1:end_frame)-times(TimeCut+1);%set the origin of times at the time when the cantilever reaches its final (low) position.
        
        % Generate the time series of cortical tension, from the "_Tension.fig" file:
        TensionFile=dir([path0 '*' fname(1:6)  '*_Tension.fig']); TensionFile=TensionFile(1).name;
        fig=openfig([path0, TensionFile]);
        axesObjs = get(fig, 'Children');
        dataObjs = findall(fig, '-property', 'xdata'); %handles to low-level graphics objects in axes
        xdata = get(dataObjs, 'XData');  %data from low-level grahics objects
        ydata = get(dataObjs, 'YData');
        i=find(cellfun(@length, ydata)>2 & cellfun(@length, ydata)<4000,2);
        i=i(2);ctrl=1;
        
        close all
        figure(3);
        hold off;
        Tension=ydata{i}; %correction factor of 0.817 already applied in GetTension2.m
        TensTimes=xdata{i}; deltTens=TensTimes(2)-TensTimes(1);
        [temp, indp]=max(Tension);%Find the index of the peak tension value. It corresponds to when the cantilever reaches its final (low) position.
        fac=1;%Weight of the background correction. Typically set to 1
        TensionTimeWindow=deltTens*(-20:round((times(end)-TimeCut*delt)/deltTens));
        TensionWindow=Tension(indp(end)-20:(indp(end)+round((times(end)-TimeCut*delt)/deltTens)));
        temp=(mean(int_cort_mean_data_all(1:length(times),40:120),2)-fac*para_cortex(5))./(intens_inner_all(1:length(times))-fac*para_cortex(5));%cortical intensity ratio, with background correction

        [AX,H1,H2] = plotyy(smooth(times, 3), smooth(rmoutliers(temp),3), TensionTimeWindow,TensionWindow);
        set(AX(1),'NextPlot','add');
        hold on ; plot(AX(1),times, temp,'bo');
        set(get(AX(1),'Ylabel'),'String','Cortical fluorescence (a.u.)');hold on;
        set(get(AX(2),'Ylabel'),'String','Cortical Tension (mN/m)');hold on;
        set(H1,'LineWidth',2)
        set(H2,'LineWidth',2)
        title(fname)
        saveas(gcf, [path0 fname(1:end-4) 'Tension_OuterOverInnerRingFluo4a.fig'])
        
        PlotIntensities(intens_all,figure(1),1, bleachrate);%generate the kymograph of cortex intensities
        savefig([path0 fname,'_int_kymograph.fig']);
    end

    function FFT=FourierAnalys()
        %This function can optionally be used to analyse the intensity
        %around the cortex using Fourier transform.
        FFT=0*intens_all;
        figure(2); hold off;
        PlotIntensities(movmean(intens_all,5),figure(1),1, bleachrate);
        colormap default
        title(fname)
        savefig([path0 fname,'_int_kymograph.fig']);
        
        for i=1:size(intens_all,1)
            FFT(i,:)=abs(fft(intens_all(i,:)/sum(intens_all(i,:))));
        end
        figure(1); hold off; contourf(movmean(FFT(:,2:10),3),20,'edgecolor','none')
        title(fname)
        saveas(gcf, [path0 fname(1:end-4) 'FourierTransform.fig'])
    end

    function Fitcortex(i)
        %This function fits the cortex radial intensity profile at frame i.
        int_cort = int_cort_mean_data_all(i,:);
        
        s=linspace(0,1,num_linep);
        [int_cort_fit,para_cortex,int_cort_fkt, int2_cort_fkt, skewgau,err] = FitCortexIntensity(int_cort);
        int_cort_fit_cyto = mean(int_cort(1:5));%int_cort_fit(1);
        intens_inner_mean = intens_inner_all(i);
        
        % cell areas and volume from time span before bleaching
        if i==1
            area_cross_inner_mean = area_cross_inner_all(i);
            area_ring_mean = area_ring_all(i);
            R2_BC=sqrt((area_cross_inner_mean+area_ring_mean)/pi);
            area_cell_mean = Area(R2_BC', h/2, h); % total cell area
            
            vol_inner_mean = Volume(sqrt(area_cross_inner_mean/pi), h, h/2);
            vol_cell_mean = Volume(R2_BC', h, h/2);
            vol_cort_mean = vol_cell_mean-vol_inner_mean;
            peri_cell_mean =peri_cell_all(i);
        end
        
        
        % Cytosol intensities from time span before bleaching
        % crosssection
        cyto_inner_2D = area_cross_inner_mean * (intens_inner_mean-para_cortex(5));
        cyto_ring_2D = area_ring_mean * (int_cort_fit_cyto);
        cytosol_2D(i) = cyto_inner_2D + cyto_ring_2D;
        
        % 3D cell from time span before bleaching
        cyto_inner_3D = vol_inner_mean * (intens_inner_mean-para_cortex(5));
        cyto_ring_3D = vol_cort_mean * (int_cort_fit_cyto-para_cortex(5));
        cytosol_3D(i) = cyto_inner_3D + cyto_ring_3D;
        
        % Cortex from time span before bleaching
        integr_gau = trapz(s,skewgau(para_cortex,s))*px*len_lines;
        cortex_2D(i) = peri_cell_mean * integr_gau; % crosssection
        cortex_3D(i) = area_cell_mean * integr_gau; % 3D cell
        
        figure(2); hold off;
        plot(s*len_lines*px,int_cort); hold on
        plot(s*len_lines*px,int2_cort_fkt(para_cortex,s));
        legend('mean data','Fit');
        title(['Averaged radial Intensity profile of cell cortex, mean for frames 1-',...
            num2str(bleach_frame),' and whole cell perimeter']);
        
    end

    function Vol=Volume(R2,Z, R1)
        if Z>(2*R2)
            Z=2*R2; R1=R2;
        end
        Vol=pi*(Z.*(2*R1.^2 -2*R1.*R2 + R2.^2-Z.^2/12)+(2*asin(0.5*Z./R1)+sin(2*asin(0.5*Z./R1))).*(R2 - R1).*R1.^2);
    end

    function A=Area(R2, R1, Z)
        A=pi*(4*R1.*(R2 - R1).*(asin(0.5*Z./R1)+cos(asin(0.5*Z./R1))-1)+2*R1.*Z+2*R2.^2-Z.^2/2);
    end

    function [int1, int2, times]=GetRecovery(FRAPfile)
        hold off;
        fig=openfig([path, direc, '/',FRAPfile(1:((strfind(FRAPfile,' '))-1)),'_recovery.fig']);
        axesObjs = get(fig, 'Children');
        dataObjs = findall(fig, '-property', 'xdata'); %handles to low-level graphics objects in axes
        xdata = get(dataObjs, 'XData');  %data from low-level grahics objects
        ydata = get(dataObjs, 'YData');
        times=xdata{1};
        recovery_corr=(ydata{4}-ydata{4}(1))/(1-ydata{4}(1));
        recovery_uncorr=(ydata{3}-ydata{3}(1))/(1-ydata{3}(1));
        int1=recovery_corr;
        int2=recovery_uncorr;
    end
 
    function ClVec=rmoutliers(vec)
        %This function removes the outliers from a vector array "vec" and returns its "clean" version.
        %It is used to plot smoother intensity ratio curves.
        for i=1:length(vec)
            if vec(i)>1.05*median(vec(max(1,i-2): min(length(vec),i+2))) || vec(i)<0.95*median(vec(max(1,i-2): min(length(vec),i+2)))
                ClVec(i)=median(vec(max(1,i-5): min(length(vec),i+5)));
            else
                ClVec(i)=vec(i);
            end
        end
    end

end

