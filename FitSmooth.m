function [X,Y,R] = FitSmooth(X_bound,Y_bound,num_pnt)
% Determines boundary curve from cell boundary points using smoothing and
% interpolation at defined points with uniform distance in arclength.

len_XY = length(X_bound);

% joined arrays with 3 times the boundary arrays to avoid errors with
% smooth and interpolation at endpoints of array:
X_bound_3 = [X_bound;X_bound;X_bound];
Y_bound_3 = [Y_bound;Y_bound;Y_bound];

% smooth:
X_smooth_3 = smooth(X_bound_3,80);%One might want to try with 200, or with the method 'rloess'
Y_smooth_3 = smooth(Y_bound_3,80);

% arclength elements:
X_smooth = X_smooth_3(len_XY+1:2*len_XY);
Y_smooth = Y_smooth_3(len_XY+1:2*len_XY);
dist_points_o1 = (sqrt((X_smooth(2:end)-X_smooth(1:end-1)).^2 ...
    + (Y_smooth(2:end)-Y_smooth(1:end-1)).^2))';
dist_points = [sqrt((X_smooth(1)-X_smooth(end)).^2 ...
    + (Y_smooth(1)-Y_smooth(end)).^2),dist_points_o1];

% arclength parametrisation:
para_circle = cumsum(dist_points);
para_circle_3 = [para_circle,...
    para_circle + para_circle(end),...
    para_circle + 2*para_circle(end)];
% delete duplicated points:
[para_circle_3_u, ind] = unique(para_circle_3);

% interpolation points:
pkt_interpol = linspace(0,para_circle(end),num_pnt);
pkt_interpol_3 = [pkt_interpol,pkt_interpol+pkt_interpol(end),...
    pkt_interpol+2*pkt_interpol(end)];

% interpolation:
X_interpol_3 = interp1(para_circle_3_u,X_smooth_3(ind),pkt_interpol_3);
Y_interpol_3 = interp1(para_circle_3_u,Y_smooth_3(ind),pkt_interpol_3);

% extract middle array from array with 3 interpolated arrays:
X = (X_interpol_3(num_pnt+1:2*num_pnt))';
Y = (Y_interpol_3(num_pnt+1:2*num_pnt))';

x_m = mean(X);
y_m = mean(Y);
R = sqrt((X-x_m).^2+(Y-y_m).^2);