function [X_orth,Y_orth] = OrtLineEndpoints(X,Y,len)
% Calculates end points of orthogonal lines with the length len through 
% each point of the boundary curve (X,Y).

k = [-0.5,0.5];
X_orth = zeros(2,length(X));
Y_orth = zeros(2,length(Y));
for n = 1:length(X)
    switch n
        case 1          % starting point
            X_v = X(end); Y_v = Y(end);
            X_n = X(n+1); Y_n = Y(n+1);
        case length(X) % end point
            X_v = X(n-1); Y_v = Y(n-1);
            X_n = X(1); Y_n = Y(1);
        otherwise       % all other points
            X_v = X(n-1); Y_v = Y(n-1);
            X_n = X(n+1); Y_n = Y(n+1);
    end   
    D(1) = X_n - X_v; D(2) = Y_n - Y_v; % slope
    D = D/norm(D); D_neu(1) = D(2); D_neu(2) = -D(1); % orthogonal slope
    X_orth(:,n) =  X(n)+k*len*D_neu(1);
    Y_orth(:,n) =  Y(n)+k*len*D_neu(2);
end