function [Fit_all,shift] = CompCellRotation(Fit_all,fnum)
% Corrects rotational orientation of boundary curve according to phase
% shift of sine and cosine

% Extract function parameters:
X_Fit = Fit_all{fnum,1};
Y_Fit = Fit_all{fnum,2};
R_Fit = Fit_all{fnum,3};

% relocate 'circle' of boundary fit:
X_M = mean(X_Fit);
Y_M = mean(Y_Fit);
X = X_Fit - X_M;
Y = Y_Fit - Y_M;

% check vector dimensions
dims_X = size(X);
[len, direction] = max(dims_X);
switch direction
    case 1
        s = (1:len)';
    case 2
        s = (1:len);
end

% define functions for fit:
cosine = @(p,s)-p(1).*cos(2*pi/len*(s-p(2)));
sine = @(p,s)-p(1).*sin(2*pi/len*(s-p(2)));

% start parameters:
amp = max(X);
para_x_0 = [amp 0]; lb=[-1000 -2*len]; ub=[1000 2*len];
para_y_0 = [amp 0];

% fit:
opt = optimoptions('lsqcurvefit','Display','off');
try
para_x = lsqcurvefit(cosine,para_x_0,s,X,lb,ub,opt);
para_y = lsqcurvefit(sine,para_y_0,s,Y,lb,ub,opt);
% compensation shift = -1 * mean of phase shifts
shift = -round(0.5*(para_x(2)+para_y(2)));
catch
    shift=0;
end


% rotate arrays for boundary fit and intensity values with determined shift
% value:
X_Fit_shift = circshift(X_Fit,shift);
Y_Fit_shift = circshift(Y_Fit,shift);
R_Fit_shift = circshift(R_Fit,shift);

Fit_all{fnum,1} = X_Fit_shift;
Fit_all{fnum,2} = Y_Fit_shift;
Fit_all{fnum,3} = R_Fit_shift;