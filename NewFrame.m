function [DataAll,BleachReg,AreaVols] ...
    = NewFrame_NoGUI(handles,ImgProp,DisPara,boundfit_method,DataAll,BleachReg,AreaVols,sens)

%global num_timescales
% Function with main calculation for each frame:

% Determines cell boundary points and boundary curve using 'FindBound',
% Uses sub-routines: 'FitSmooth', 'FitCircle', 'CorrectBoundFit', 'CompCellRotation'.

% Determines intensity profiles and values using 'FrameIntensities'.

% Determines location of bleach region using 'FindBleachRegion'.

% Ensures correct rotational orientation of all data arrays.

% Displays current frame with boundary curve.

% Extract function parameters:
% Arrays with data for all frames:
bounds_all = DataAll{1}; bound_fit_all = DataAll{2};
intens_all = DataAll{3}; fit_bleach_all = DataAll{4};
intens_cort_mean_data_all = DataAll{5}; intens_inner_all = DataAll{6};
int_total=DataAll{7};
% Image properties:
path = ImgProp{1}; file = ImgProp{2}; px = ImgProp{3};
fnum_max = ImgProp{4}; fnum = ImgProp{5}; bleach_frame = ImgProp{6};
% Discretisation parameters:
num_fitp = DisPara{1}; num_linep = DisPara{2}; len_lines = DisPara{3};
sigma = DisPara{4};
% Bleach Region parameters:
intens_bl_reg_gau = BleachReg{1};
intens_bl_reg_erf = BleachReg{2};
ind_bl_reg_mid = BleachReg{3};
reg_lb_ind = BleachReg{4};
reg_ub_ind = BleachReg{5};
intens_bl_reg_int = BleachReg{6};
intens_bl_reg = BleachReg{7};
index_rotation = BleachReg{8};
intens_bl_reg_radial= BleachReg{9};								   
% Cell dimensions:
area_cross_inner_all = AreaVols{1};
area_ring_all = AreaVols{2};
vol_inner_all = AreaVols{3};
vol_cort_all = AreaVols{4};
peri_cell_all = AreaVols{5};
area_cell_all = AreaVols{6};
vol_cell_all = AreaVols{7};

tiffile = dir([file(1:end-3),'tif']);

if length(tiffile)>0
    im_array = imread([path, char(tiffile(1).name)],fnum);
else
    im_array_0 = imread([path, file],2*fnum-1);
    im_array = im_array_0(:,:,1);
end

if fnum==bleach_frame
    if length(tiffile)>0
        im_arrayprev = imread([path, char(tiffile(1).name)],fnum-1);
    else
        im_array_0 = imread([path, file],2*fnum-3);
        im_arrayprev = im_array_0(:,:,1);
    end
end

try
set(handles.frame_text,'String',...
    ['Frame ',num2str(fnum),'/',num2str(fnum_max)]);
set(handles.video_slider,'Value',fnum);
catch
end

if fnum==10
end
if fnum~=bleach_frame
    [X_bound,Y_bound, int_total_fr] = FindBound(im_array,sigma, sens);
else
    [X_bound,Y_bound, int_total_fr] = FindBound(im_arrayprev,sigma, sens);
end
bounds_all{fnum,1} = X_bound;
bounds_all{fnum,2} = Y_bound;

if fnum==bleach_frame
end
% Determine boundary curve:
switch boundfit_method
    case 'smooth'
        [X_Fit,Y_Fit,R_Fit]=FitSmooth(X_bound,Y_bound,...
            num_fitp);
    case 'circle'
        [X_Fit,Y_Fit,R_Fit]=FitCircle(X_bound,Y_bound,num_fitp);
end

bound_fit_all{fnum,1} = X_Fit;
bound_fit_all{fnum,2} = Y_Fit;
bound_fit_all{fnum,3} = R_Fit;

correct=0;
if fnum > 1
    % check, if bound curve is realistic:
    [bound_fit_all,correct] = CorrectBoundFit(bound_fit_all,bounds_all,fnum);
    % for all frames after bleach frame: if current boundary fit
    % =/= last boundary fit: rotate bound fit arrays for current
    % frame
end
if correct == 0 && (index_rotation>0)
    bound_fit_all{fnum,1} = circshift(bound_fit_all{fnum,1},index_rotation);
    bound_fit_all{fnum,2} = circshift(bound_fit_all{fnum,2},index_rotation);
    bound_fit_all{fnum,3} = circshift(bound_fit_all{fnum,3},index_rotation);
end

% Compensate cell rotation:
[bound_fit_all,~] = CompCellRotation(bound_fit_all,fnum);
X_Fit = bound_fit_all{fnum,1};
Y_Fit = bound_fit_all{fnum,2};
Rapprox=mean(R_Fit);
meanx=mean(X_Fit);
meany=mean(Y_Fit);


% determine intensities for each point of boundary fit, intensities
% of cortex and cytosol for first frames:
[intens_frame,int_cort_mean_data,int_cort_data, AreaVol,intens_inner_fr_mean] = FrameIntensities(im_array,X_Fit,Y_Fit,num_linep,fnum,px,...
    len_lines,bleach_frame);

intens_all(fnum,:) = intens_frame;
intens_cort_mean_data_all(fnum,:) = int_cort_mean_data;
int_total(fnum,:)=int_total_fr;%mean(im_array(:));

area_cross_inner_all(fnum) = AreaVol{1};
area_ring_all(fnum) = AreaVol{2};
vol_inner_all(fnum) = AreaVol{3};
vol_cort_all(fnum) = AreaVol{4};
peri_cell_all(fnum) = AreaVol{5};
area_cell_all(fnum) = AreaVol{6};
vol_cell_all(fnum) = AreaVol{7};

intens_inner_all(fnum) = intens_inner_fr_mean;

% fit intensity for bleach frame to determine bleach
% region
if fnum >= bleach_frame
    % determine bleach region only for bleach frame:
    if fnum == bleach_frame
        para0 = [1,1,1,1];
        [fit_bleach,fit_bleach_ftn,para_bleach_ftn,index_rotation] = FindBleachRegion(intens_all,fnum,para0,bleach_frame);
        reg_lb_2 = para_bleach_ftn(2)-0.5*para_bleach_ftn(3)-4;
        reg_ub_2 = para_bleach_ftn(2)+0.5*para_bleach_ftn(3)+4;
        reg_lb_ind = round(reg_lb_2);
        reg_ub_ind = round(reg_ub_2);
        ind_bl_reg_mid = round(para_bleach_ftn(2));
        %figure; plot(intens_all(bleach_frame, :))
        % if bound starting point was in bleach region:
        % rotate bound fit and intens array for current and
        % all previous frames:
        for n = 1:fnum
            bound_fit_all{n,1} = circshift(bound_fit_all{n,1},index_rotation);
            bound_fit_all{n,2} = circshift(bound_fit_all{n,2},index_rotation);
            bound_fit_all{n,3}  = circshift(bound_fit_all{n,3}, index_rotation);
        end
        intens_all(1:fnum,:) = circshift(intens_all(1:fnum,:),index_rotation,2);
        int_cort_data = circshift(int_cort_data, index_rotation,2);
        X_Fit = bound_fit_all{fnum,1};
        Y_Fit = bound_fit_all{fnum,2};
        meanx=mean(X_Fit); meany=mean(Y_Fit);
    end
    bl_reg_mean_data = mean(int_cort_data(:,reg_lb_ind:reg_ub_ind),2)';
    s=linspace(0,1,num_linep);
    s_m = s*len_lines*px;
	intens_cort_mean_data_all(fnum,:) = mean([int_cort_data(:,1:reg_lb_ind) int_cort_data(:,reg_ub_ind:end)],2)';%After the bleaching, this is an average only outside of the bleaching region
    intens_bl_reg(fnum-bleach_frame+1) = mean(intens_all(fnum,reg_lb_ind:reg_ub_ind));
end

if reg_ub_ind>0
    intens_bl_reg_radial(fnum,:) = mean(int_cort_data(:,reg_lb_ind:reg_ub_ind),2)';
end


% Set function return variables:
DataAll = {bounds_all,bound_fit_all,intens_all,fit_bleach_all,...
    intens_cort_mean_data_all,intens_inner_all, int_total};
AreaVols = {area_cross_inner_all,area_ring_all,vol_inner_all,...
    vol_cort_all,peri_cell_all,area_cell_all,vol_cell_all};
BleachReg = {intens_bl_reg_gau,intens_bl_reg_erf,ind_bl_reg_mid,...
    reg_lb_ind,reg_ub_ind,intens_bl_reg_int,intens_bl_reg,index_rotation,intens_bl_reg_radial};

% visualisation:

txt = ['fnum=' num2str(fnum)];
figure(1); hold off;
imagesc(im_array);
daspect([1 1 1]);% colormap(gray);
hold on
% plot(X_bound,Y_bound,'g');
plot(X_Fit,Y_Fit, 'r','LineWidth',1);
text(10,10,txt,'Color',[1 1 1]);

%plot(X_bound,Y_bound, 'b','LineWidth',1);
1
clearvars -except DataAll BleachReg AreaVols
% plot(X_Fit(1),Y_Fit(1),'xc','MarkerSize',10);