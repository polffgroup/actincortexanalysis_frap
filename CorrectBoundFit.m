function [Fit_all, correct] = CorrectBoundFit(Fit_all,bounds_all,fnum)
% Checks, if boundary curve is realistic or is shaped strangely.
% If boundary is unrealistic, take boundary curve from last frame and allow
% translation shift.

    X_Fit_0 = Fit_all{fnum-1,1};
    X_Fit_1 = Fit_all{fnum,1};
    Y_Fit_0 = Fit_all{fnum-1,2};
    Y_Fit_1 = Fit_all{fnum,2};
    X_bound_1 = bounds_all{fnum,1};
    Y_bound_1 = bounds_all{fnum,2};
    
    R_bound_0 = sqrt((X_Fit_0-mean(X_Fit_0)).^2+(Y_Fit_0-...
            mean(Y_Fit_0)).^2);
        
    R_bound_1 = sqrt((X_bound_1-mean(X_Fit_0)).^2+(Y_bound_1-...
        mean(Y_Fit_0)).^2);
        
    max_grad2 = 0.4*1.5;%factor 500^2/200^2 added, changed 1.11.19
    max_trans = 5;
    R_Fit = Fit_all{fnum,3};
    mean_R_0 = mean(R_bound_0);
    min_R = 0.7*mean_R_0;
    R_Fit_grad2 = gradient(gradient(R_Fit));
    
    correct = (max(abs(R_Fit_grad2))>max_grad2) || (min(R_bound_1)<min_R);
    
    if correct
        
        % matrices with distances between all points:
        diff_X = distCoord(X_Fit_0,X_Fit_1);
        diff_Y = distCoord(Y_Fit_0,Y_Fit_1);

        D = sqrt(diff_X.^2+diff_Y.^2);
        d = min(D,[],1);                % distances to nearest points
        
        % use only points with small distance to next point
        use = logical(d<max_trans);
        
        % x and y distances:
        dx = minabs(diff_X,1);
        dy = minabs(diff_Y,1);
        
        med_dx = median(dx(use));
        med_dy = median(dy(use));

        Fit_all{fnum,1} = Fit_all{fnum-1,1} - med_dx;
        Fit_all{fnum,2} = Fit_all{fnum-1,2} - med_dy;
    end

    function diff_K = distCoord(K0,K1)
        % determine distances between all points of K0 and K1
        dims_K = size(K0);
        [~, direction] = max(dims_K);
        if direction == 2
            K0 = K0';
            K1 = K1';
        end
        M_K0 = repmat(K0,1,length(K0));
        M_K1 = repmat(K1',length(K1),1);
        diff_K = M_K0 - M_K1;
    end

    function ma = minabs(M,dim)
        % calculate elements with the minimal absolute values
        maM = min(abs(M),[],dim);
        i_minabs_neg = (M+maM==0);
        i_minabs_pos = (M-maM==0);
        i_minabs = logical(i_minabs_neg+i_minabs_pos);
        ma = M(i_minabs)';
    end
end