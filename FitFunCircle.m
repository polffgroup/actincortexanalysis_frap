function ret = FitFunCircle(para, phi, Data)
% Defines main fit function for circle fit.
% Uses function 'Circle_xy'.
ret = [];
[x, y] = Circle_xy(para,phi);
C = min((Data(:,1)-x).^2+(Data(:,2)-y).^2);
ret = [ret C];
end