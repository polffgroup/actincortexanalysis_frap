function [intens_frame,int_cort_mean_data,int_cort_data,AreaVol, intens_inner_fr_mean] ...
    = FrameIntensities(im_array,X_Fit,Y_Fit,num_linep,fnum,px, len_lines,bleach_frame)
% Determines intensity profiles of cell boundary, calculates intensity
% value for each point of the boundary curve.
% Used in NewFrame.m (itself used in AnalyseFRAPcurves.m)
 
% Endpoints of orthogonal lines at every point of boundary fit:
[X_Lines,Y_Lines] = OrtLineEndpoints(X_Fit, Y_Fit, len_lines);

plot(X_Fit, Y_Fit,'r-');
plot(X_Lines(:,1:10:end), Y_Lines(:,1:10:end),'r-');


s = linspace(0,1,num_linep);
num_fitp = length(X_Lines(1,:));
int_cort_data = zeros(num_linep,num_fitp);
intens_frame = zeros(1,num_fitp);

% loop over every point of boundary fit:
for fitp = 1:num_fitp
    % intensity profile of cortex along orthogonal line:
    try
     [cx, cy, int_cort_data(:,fitp)] = improfile(im_array, [X_Lines(1,fitp) X_Lines(end,fitp)],...
        [Y_Lines(1,fitp) Y_Lines(end,fitp)],num_linep);
    catch
        int_cort_data(:,fitp)=NaN(num_linep,1);
        1
    end
    
    
    % if lines exceed image dimensions, intensity is NaN at these points
    NaN_logic = isnan(int_cort_data(:,fitp));
    % set these intensities to zero:
    int_cort_data(NaN_logic,fitp) = 0;

    % trapez integration along orthogonal line, convert to µm-units
    intens_frame(fitp) = trapz(s(40:120),int_cort_data((40:120),fitp))*px*len_lines;%The range 40:120 must contain the fluorescence peak of the actin cortex.
	
    try
    if mod(fitp,10)==1
        plot(cx(40:120),cy(40:120),'y');
    end
    catch
    end
end

% for frames before bleaching:
if fnum < bleach_frame
    % mean intensity distribution along radius for all boundary points:
    int_cort_mean_data = mean(int_cort_data,2)';

    % get effective radii (cell and inner) for area and volume calculation:
    x_M = mean(X_Fit); y_M = mean(Y_Fit);
    R_Fit = sqrt((X_Fit-x_M).^2+(Y_Fit-y_M).^2);
    R_eff = mean(R_Fit);
    phi = 2*pi*(1:length(X_Fit))./length(X_Fit);
    f = 0.7;%Relative radius of the region considered "inner" cytoplasm. Typically set to 0.7
    X_inner = zeros(1,length(X_Fit));
    Y_inner = zeros(1,length(Y_Fit));

    for n=1:length(X_Fit)
        X_inner(n) = -f*R_Fit(n)*cos(phi(n))+x_M;
        Y_inner(n) = -f*R_Fit(n)*sin(phi(n))+y_M;
    end
    plot(X_inner, Y_inner,'r-');%plot the countour of the inner cytoplasm
    
    R_i_eff = f*R_eff;
    x_M_i = mean(X_inner); y_M_i = mean(Y_inner);

    % Determine inner intensity:
	
    [X,Y] = meshgrid(1:size(im_array,2),1:size(im_array,1)); 
    R = @(x,y,x_m,y_m)sqrt((x-x_m).^2 + (y-y_m).^2);

    % logical array: pixel within inner radius = 1, otherwise 0:
    logic_inner = (R(X,Y,x_M_i,y_M_i)<=R_i_eff);

    % number of inner pixels:
    num_inner = length(im_array(logic_inner));

    % image array with outer pixels = 0:
    im_inner = double(im_array).*double(logic_inner);

    % add up inner intensities and divide by number of pixels:
    intens_inner_fr_mean = sum(sum(im_inner))/num_inner;

    % convert radii to µm:
    R_eff_mym = R_eff*px;                   % µm
    R_i_eff_mym = R_i_eff*px;               % µm

    % calculate areas and volumes:
    % cell crosssection:
    area_cross_cell = pi * R_eff_mym^2;              % µm^2
    area_cross_inner = pi * R_i_eff_mym^2;           % µm^2
    area_ring = area_cross_cell - area_cross_inner;  % µm^2
    % area of 3D-cell:
    area_cell = 4*pi * R_eff_mym^2;                  % µm^2
    % perimeter of crosssection:
    peri_cell = 2*pi * R_eff_mym;                    % µm
    % cell volume:
    vol_cell = (4/3)*pi * R_eff_mym^3;               % µm^3
    vol_inner = (4/3)*pi * R_i_eff_mym^3;            % µm^3
    vol_cort = vol_cell - vol_inner;                 % µm^3

    % Cell array for function output:
    AreaVol = {area_cross_inner,area_ring,vol_inner,vol_cort,peri_cell, area_cell,vol_cell};
end
if fnum > (bleach_frame-1)
    int_cort_mean_data = 0;
    intens_inner_fr_mean = 0;
    int_cort_fit = 0;
    AreaVol = {0,0,0,0,0,0,0};
end

