function [int_cort_fit,para,int,int2, skewgau,err] = FitCortexIntensity(int_cort_mean_data)
% Determines cortex and cytosol portions of the intensity on the cell
% boundary by fitting intensity profile to  gauss function (cortex) + 
% error function (cytosol)

num_linep = size(int_cort_mean_data,2);
s=linspace(0,1,num_linep);

% determine start parameters for fit:
% maximum of cortex peak:
[max_peak,ind_peak]=max(int_cort_mean_data);
% background intensity on the outside of the cell:
int_aussen = mean(int_cort_mean_data(round(0.8*num_linep):end));
% height of error function:
int_cort_innen = mean(int_cort_mean_data(1:round(0.1*num_linep)));

% fit functions:
err = @(p,x)p(1)*0.5*(1-erf((x-p(2))/(sqrt(2)*p(3))))+p(5);
gau = @(p,x)p(4)*exp(-((x-p(2)).^2)/(2*p(3).^2));
skewgau=@(p,x) gau(p,x).*(0.5*(1+erf(p(6)*(x-p(2))/(sqrt(2)*p(3)))));
int = @(p,x)err(p,x)+gau(p,x);
int2= @(p,x)err(p,x)+skewgau(p,x);

% Fit parameters:
para0 = [int_cort_innen ind_peak 1 max_peak int_aussen];
lb = [0 0 0 0 0];
ub = [2*max_peak 1 100 2*max_peak 2*int_aussen];
% fit:
opt = optimoptions('lsqcurvefit','Display','off');
para = lsqcurvefit(int,para0,s, int_cort_mean_data,lb,ub,opt);
%fit skewed gaussian
para0 = [para -1];
lb = [0 0 0 0 0];
ub = [2*max_peak 1 100 2*max_peak 2*int_aussen];
% fit:
opt = optimoptions('lsqcurvefit','Display','off');
para = lsqcurvefit(int2,para0,s(1:round(0.75*num_linep)), int_cort_mean_data(1:round(0.75*num_linep)),lb,ub,opt);
%para = lsqcurvefit(int2,para0,s, int_cort_mean_data,lb,ub,opt);%Test this if there are visible microvilli around the cell																											 
int_cort_fit = int2(para,s);