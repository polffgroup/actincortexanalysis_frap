function [fit,errfkt,para,index_rotation] = FindBleachRegion(...
    intens_all,fnum,para0,bleach_frame)
% Detects bleach region for first frame after bleaching by fitting 2 error
% functions

% fit function:
errfkt = @(p,x)p(4)-p(1)*0.5*(erf(x-p(2)+0.5*p(3))-erf(x-p(2)-0.5*p(3)));

pnt = 1:size(intens_all,2);

fits = cell(2,1);
paras = cell(2,1);
fit_quality = zeros(1,2);
ind_rots = [0,round(size(intens_all,2)/2)];

[i1 i2]=size(intens_all)
M = ceil(i2/25); % minimal index width of bleach region

% do erf-fit for original and for rotated intensity array
for m = 1:2
    try
        intens_all_rot = circshift(intens_all,ind_rots(m),2);
        intens_rot = intens_all_rot(fnum,:);
        if fnum == bleach_frame
            
            intens_bl = intens_all_rot(bleach_frame,:);   % int. bleach frame
            intens_pre = intens_all_rot(bleach_frame-1,:);% int. before bleach
            
            % Smooth Data
            intens_bl_smooth = smooth(intens_bl,20);
            intens_pre_smooth = smooth(intens_pre,20);
            
            % parameters bleach frame and previous frame:
            min_pre_s = min(intens_pre_smooth);
            mean_bleach_s = mean(intens_bl_smooth);
            [min_bleach_s,~] = min(intens_bl_smooth./intens_pre_smooth);
            temp=find((intens_bl_smooth./intens_pre_smooth)<0.7);
            x_reg_end=[max(1,temp(1)) min(temp(end-1), length(intens_bl))];
            
            % fit parameters:
            mid_reg = 0.5*(x_reg_end(1)+x_reg_end(2));
            width_reg = x_reg_end(2)-x_reg_end(1);
            y0_0 = mean_bleach_s; y_max_0 = abs(mean_bleach_s - min_bleach_s);
            para0 = [y_max_0 mid_reg width_reg y0_0];
            % for fnum > bleach_frame: para0 = para of previous fit
        end
        % fit boundaries:
        lb_e = [0 para0(2)-0.5*para0(3) M 0];
        ub_e = [2*para0(1) para0(2)+0.5*para0(3) 1.5*para0(3) 2*para0(4)];
        
        % fit:
        paras{m} = lsqcurvefit(errfkt,para0,pnt,intens_rot,lb_e,ub_e);
        %figure; plot(pnt, intens_rot, pnt, errfkt(paras{m}, pnt));
        fits{m} = errfkt(paras{m},pnt);
        mean_intens = mean(intens_rot);
        fit_quality(m) = 1-(sum((fits{m}-intens_rot).^2)./...
            sum((intens_rot-mean_intens).^2));
    catch
        fit_quality(m)=0;
    end
end

% choose best fit for bleach region:
[best_fit_quality, best_fit] = max(fit_quality);
fit = fits{best_fit};
para = paras{best_fit};
if abs(para(2)-100)>70 %to avoid errors in some difficult cases
    best_fit=mod(best_fit,2)+1;
    fit = fits{best_fit};
    para = paras{best_fit};
end
if length(para)==0
end				  
index_rotation = ind_rots(best_fit);