function PlotIntensities(intens_all,intens_fig,smooth_yn,bleach_rate)
% Plots intensity kymograph (intensities for all frames and angles).

frames = 1:length(intens_all(:,1));
pnt_lines = 1:length(intens_all(1,:));
% set axis limits:
limits=[0,length(intens_all(1,:)),1,length(intens_all(:,1))];
if smooth_yn == 1
    for n = frames
        intens_all(n,:) = smooth(intens_all(n,:),5);
    end
end
% bleaching correction
temp=intens_all;
for n = frames
    temp(n,:) = intens_all(n,:)*exp(n*bleach_rate);
end
intens_fig;
%[xWinkel,yFrames] = meshgrid(pnt_lines,frames);
%mesh(xWinkel,yFrames,intens_all);
imagesc(smoothn(temp));
colormap(hot);
title('Intensity kymograph');
axis(limits);
xlabel('arclength parameter'); ylabel('frame');
