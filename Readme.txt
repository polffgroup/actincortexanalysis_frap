#####################
ANALYSIS OF FRAP DATA
#####################
Readme to be able to do the analysis of FRAP confocal time series of mitotic cells with and without accompanying AFM recordings

Main script is: "AnalyseFRAPcurves.m"
Input files: a confocal time series in .lsm format and (optional) an AFM readout (JPK software, real time scan) in format *.out .

Confocal files and AFM files should both be named such that the name starts with Cellxx_Label_....lsm or .out
Example:
Cell1_Ctrl_HelaMitosis_STC2uM.lsm  and
Cell1_Ctrl_HelaMitosis_STC2uM.out

OR

Cell1_Bleb10uM_HelaMitosis_STC2uM.lsm  and
Cell1_Bleb10uM_HelaMitosis_STC2uM.out

For this measurement, CtrlLabel='Ctrl', CondLabel='Bleb10uM'


After recording FRAP data:

Run the script "AnalyseFRAPcurves.m" with correctly set parameters at the top.
This script uses the sub-routines: WriteFileHead, NewFrame.

This generates an output file "AFMwConfocAna.txt". And, for every cell analysed, there are five additional output files in the matlab figure format (recovery curve, kymograph, cell boundary, intensity profile in radial direction across the circumference, cortical tension over time).

In particular finding the right sensitivity parameter (“sens”) for the boundary recognition of the cells might need some adjustment, see sub-routine FindBound.m.
If cells show a lot of movement, run the Fiji script "Template matching/Align slices in stack" first. Save the aligned movie in a tiff file with equal name.
Then the analysis software will automatically use the tiff file instead of the lsm file.


After analysing all cells in the measurement, you can produce an averaged recovery curve and a statistical analysis with the script "AnalysisScriptSummary.m" 
You can also pool here results from several measurement days by giving several directories in the list "DirectoryLabels".
Input file in every directory to be analysed is AFMwConfocAna.txt
Output files are matlab figures "TauVsTension_LabelString.fig" (scatter plot of recovery times versus measured cortical tension) and "AveragedRecovery_LabelString.fig" (First panel: averaged recovery curves, boxplots of measured cortical tension in control and condition, boxplots of fitted recovery times in control and condition, boxplots of fitted cortical fractions in control and condition)


Fit functions used here, in LateX format:
	- Cell cortex radial intensity: I(x)= I_i*0.5*(1-erf((x-x0)/(sqrt(2)*sigma)))+I_g*exp(-((x-x0)^2)/(2*sigma^2))*(0.5*(1+erf(skew*(x-x0)/(sqrt(2)*sigma))))+I_off
	where "erf" is the error function.
	- Intensity recovery after photobleaching: I(t)=-amp*exp(-t/tau)+offset


Note: In the AFM file, the force signal must be recorded without the 0.817 correction factor used for rectangular cantilevers. This "apparent force" signal is then corrected in the sub-routine GetTension.
The same is true for the apparent cantilever stiffness "Kcant" provided by the user at the top of AnalyseFRAPcurves.m. See GetTension.m.






################################################################################
ANALYSIS OF DATA from "CELL SQUISHING" or "BLUE LIGHT PHOTOACTIVATION OF MYOSIN"
################################################################################
Readme to be able to do the analysis of confocal image time series with and without accompanying AFM recordings, acquired during "cell squishing" and "blue light photoactivation of myosin" experiments.

Main script: "AnalyseSquishing.m"
Input files: a confocal time series in both .lsm and .tif formats, and and (optional) an AFM readout (JPK software, real time scan) in format *.out .

This procedure also works for "blue light photoactivation of myosin" experiments done without AFM measurements.

Confocal files and potential AFM files should be named in the same manner as for FRAP data (see above).


After recording the data:

Open the .lsm files in Fiji and do the following edits:
    - Optionnally, crop the images into a smaller square containing the cell, for a more robust cell boundary recognition.
	- Optionally, if cells show a lot of movement (typically in experiments without AFM), one can run the Fiji script "Template matching/Align slices in stack".
    - For "cell squishing" experiments, remove the frames acquired before the cantilever reached its final lower position (end of the squishing ramp).
Then save the image time series as .tif files, with the same names as the .lsm files.

Run the script "AnalyseSquishing.m" with correctly set parameters at the top.
In particular finding the right sensitivity parameter (“sens”) for the boundary recognition of the cells might need some adjustment, see sub-routine FindBound.m.
This script uses the sub-routines: FindBound, FitSmooth, GetTension2, CompCellRotation, FrameIntensities2.

This generate four output files for each analysed cell:
    - a time series of the cortical intensity ratio values and, potentially, the cortical tension values during confocal imaging [FileName "_Tension_OuterOverInnerRingFluo5a.fig"] or [FileName "_OuterOverInnerRingFluo5a.fig"]
    - a time series of the cortical tension and cell confinement height values [FileName "_Tension.fig"]
    - a kymograph of cortical fluorescence intensity over time [FileName "_int_kymograph.fig"]
    - a MAT file containing all the variables created in this script [FileName "d.mat"].

The "_OuterOverInnerRingFluo5a.fig" files can be used to create averaged curves of cortical intensity ratio and tension over time, using the script: "AverageCurves.m".
If no AFM data is present, the averaged cortical intensity ratio is made with "AverageCurves_noAFM.m".
In both cases, all the "_OuterOverInnerRingFluo5a.fig" files to be used in averaging must be in the same folder.

Note: Just as above, in the AFM file, the force signal must be recorded without the 0.817 correction factor used for rectangular cantilevers. This "apparent force" signal is then corrected in the sub-routine GetTension.
The same is true for the apparent cantilever stiffness "Kcant" provided by the user at the top of AnalyseSquishing.m. See GetTension.m.
