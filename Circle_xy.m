function [x, y] = Circle_xy(p,phi)
% Defines function for x and y coordinates of circle with fourier modes

% fit parameters:
R = p(1);
A=p(2); B=p(3); C=p(4); D=p(5);% E=p(8); F=p(9); %G=p(10); H=p(11);
% calculate sine and cosine functions beforehand:
s=sin(phi); c=cos(phi); s2=sin(2*phi); c2=cos(2*phi);
s3=sin(3*phi); c3=cos(3*phi); s4=sin(4*phi); c4=cos(4*phi);
% calculate x and y coordinates:
x = -R.*c.*(1 + A*c + B*s + C*c2 + D*s2);% + E*c3 + F*s3 + G*c4 + H*s4);
y = -R.*s.*(1 + A*c + B*s + C*c2 + D*s2);% + E*c3 + F*s3 + G*c4 + H*s4);
end
