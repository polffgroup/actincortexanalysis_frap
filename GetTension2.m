function [ConfHeight,TensVal]=GetTension2(path,  Kcant, cantspike, Imaging_fname, Aproj)
%Determines the cell confinement height and the tension value over time.
%Creates the '_Tension.fig' figure.
%Uses function readsinAFM2_rawdata								   
global offset; 

R2_BC=sqrt(Aproj/pi);
allAFMfiles=dir([path   Imaging_fname(1:6) '*.out']);

fname=allAFMfiles(end).name; %picks the last AFM file only, in case there are several ones
[times,Zs, Fs]=readsinAFM2_rawdata([path],fname,3); %Fs here is in nN, and Zs in µm
delt=times(2)-times(1);
i1=strfind(fname,'-'); i1=i1(2);
timestampAFM=((str2num(fname([i1+1,i1+2])))*3600+str2num(fname(i1+4:i1+5))*60+str2num(fname(i1+7:i1+8)));
Bleachtimes=round(length(Zs)/2)*delt;

[offset_temp ind]=min(Zs); ind=500;
if offset_temp<1 %Adapt this threshold value to the cantilever height range used on the AFM
    offset=offset_temp;
    ind=floor(50/delt);
else
    warndlg('offset was taken from previous measurement')
    %offset=-1;%Alternative: set a fix default value for offset
end


figure(2);subplot(2,1,1); hold off
[AX,H1,H2] = plotyy(times, Fs,  times, Zs);
title([fname],'Interpreter','none')
xlabel 'time (s)'
set(get(AX(1),'Ylabel'),'String','Force (nN)');hold on;
set(AX(1),'ylim',[-10 1.1*max(Fs)])
set(AX(2),'ylim',[-1 1.1*max(Zs)])
set(AX(2),'YTick',0:2:16)
set(get(AX(2),'Ylabel'),'String','Cantilever height (um)')
hold on
%Linear correction for a potential drift in the cantilever vertical deviation ("force") signal.
try
    if(abs(Fs(end)-Fs(1))>1)
        warndlg(['pretty strong force drift here! Linear correction used on ' fname(1:6)])
        Fs=Fs-(Fs(end)-Fs(1))/times(end)*times;
    end
    Fs=Fs-Fs(1);%For cases when measurement was started with a non-zero cantilever deviation value.
catch
end

start=ind+(10)/delt;%in most cases, yields start=600, i.e 600th frame, or time~60sec
times=times(start:10:end); Zs=Zs(start:10:end); Fs=Fs(start:10:end);
delt=delt*10;


Zs=Zs+Fs/Kcant+cantspike-offset;
hs=Zs;
dataL=length(times);


hrange=[hs(round(Bleachtimes/delt)) hs(round(Bleachtimes/delt)+10)];
V0=Volume(R2_BC', mean(hrange), mean(hrange)/2);
SurfArea=Area(R2_BC', mean(hrange)/2, mean(hrange)); % total cell area
R0=(3/4*V0/pi)^0.333;
fname	 
R2s=R2calc(hs, V0+0*hs);
R1s=hs/2;
xs=(R2s-R1s)+2/3*(real(sqrt(-R1s.*R2s.^2 + R2s.^3)./sqrt(R1s + R2s))-(R2s-R1s)); % radius of contact area with cantilever
Ts=Fs(1:length(hs))./(pi*xs.^2)./(1./R1s+1./R2s);%tension values before correction for rectangular cantilever
As=Area(R2s, hs/2, hs);

figure(2);subplot(2,1,2); hold off
[AX,H1,H2] = plotyy(times, 0.817*Ts,  times, Zs);  %The force correction factor of 0.817 for use of a rectangular cantilever appears here.
title([fname],'Interpreter','none')
xlabel 'time (s)'
set(get(AX(1),'Ylabel'),'String','tension (nN)');hold on;
set(AX(1),'ylim',[-0.2 1.1*max(Ts)])
set(AX(2),'ylim',[0.9*min(Zs) 1.1*max(Zs)])
set(AX(2),'YTick',0:20)
set(get(AX(2),'Ylabel'),'String','Cantilever height (um)')
hold on

range=round(length(Ts)*0.85):round(length(Ts)*0.9);
figure(2); hold on 
plot(AX(1),[ (range(1)*delt+times(1)) (range(1)*delt+times(1))], [0 0.817*median(Ts(range))]); 
saveas(gcf,[path Imaging_fname '_Tension.fig']);
close(gcf)
ConfHeight=median(hs(range)); %value of the (squished state) confinement height used in the analysis
TensVal=0.817*median(Ts(range));%Tension value calculated but not used in AnalyseSquishing.

function Vol=Volume(R2,Z, R1)
        if Z>(2*R2)
            Z=2*R2; R1=R2;
        end
        Vol=pi*(Z.*(2*R1.^2 -2*R1.*R2 + R2.^2-Z.^2/12)+(2*asin(0.5*Z./R1)+sin(2*asin(0.5*Z./R1))).*(R2 - R1).*R1.^2);
    end

    function R2=R2calc(Z, V0)
        Rs=0*Z;
        start=12;
        for i=1:length(Z)
            if Z(i)>(2*R0)
                R2(i)=R0;
            else
                R2(i)=fzero(@(x) (Volume(x,Z(i), 0.5*Z(i))-V0(i)),start);
            end
            start=R2(i);
        end
    end

    function A=Area(R2, R1, Z)
        A=pi*(4*R1.*(R2 - R1).*(asin(0.5*Z./R1)+cos(asin(0.5*Z./R1))-1)+2*R1.*Z+2*R2.^2-Z.^2/2);
    end
    function [int1, int2]=AverageRecovery(FRAPfile,indicl, RecArr_corr, RecArr_uncorr)
        fig=openfig([path,FRAPfile(1:end-1),'_recovery.fig']);
        axesObjs = get(fig, 'Children');
        dataObjs = findall(fig, '-property', 'xdata'); %handles to low-level graphics objects in axes
        xdata = get(dataObjs, 'XData');  %data from low-level grahics objects
        ydata = get(dataObjs, 'YData');
        times=xdata{1};
        recovery_corr=(ydata{4}-ydata{4}(1))/(1-ydata{4}(1));
        recovery_uncorr=(ydata{3}-ydata{3}(1))/(1-ydata{3}(1));
        if length(recovery_corr)>=length(RecArr_corr(1,:))
            int1=recovery_corr(1:length(RecArr_corr(1,:)));
            int2=recovery_uncorr(1:length(RecArr_corr(1,:)));
        else
            int1=[recovery_corr, NaN(1,length(RecArr_corr(1,:))-length(times))];
            int2=[recovery_uncorr, NaN(1,length(RecArr_corr(1,:))-length(times))];
        end
        
    end
end



