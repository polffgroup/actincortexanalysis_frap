% program input: lsm-files showing time series of a cross-section of a cell fluorescently labeled boundary 
% and an AFM readout file with the measured force and cantilever height over time (JPK output format *.out)
% The program outputs cell geometrical parameters and average measured cortical tension
% The program identifies the cell boundary at every recorded time point (script FindBound.m called by NewFrame.m)
% The program identifies automatically where the bleached region at the cortex is (script FindBleachReg.m)
% The program outputs the intensity in this bleached region over time in a file: [FileName "_recovery.fig"], it outputs the averaged radial
% intensity profile of the cortex from the frames recorded before bleaching: [FileName "cortex.fig"]
% The program outputs a kymograph of cortical fluorescence intensity over
% time in the file [FileName "kymograph.fig"]

function AnalyseFRAPcurves()
global path_o path_s file_s num_fitp num_linep len_lines len_lines_um fit_delay_time fit_end_time list_s list_c num_files
global stop bleach_frame sigma_gauss boundfit_method 
global file_no FFTarr l
global offset Kcant cantspike CondLabel CtrlLabel sens NormFac
'This is the NO GUI version'

%Parameters that should be set
num_linep = 200;        % number of points on orthog. lines along cortex, don't change this value, otherwise there will be problems
num_fitp = 200; % points for boundary fit
len_lines_um = 6; % length of orthog. lines along cortex in um
NormFac=(120-39)/200*len_lines_um;
bleach_frame=6; % frame number recorded first after bleaching
direc='201008 LSM700_FlnA';
path_o = ['yourpathname', direc,'/'];
sens=.65; %sensitivity parameters that is used in the determination of the cell boundary, see function Findbound.m
boundfit_method='smooth'; %other possibility: circle
fit_delay_time=0; %to avoid contribution from diffusion through cytoplasm
fit_end_time=350; %must be bigger than maximum durations of the videos (time in seconds)																		 

%AFM parameters
offset=-1; cantspike=0.3;  Kcant=428.8; % offset: z-coordinate of dish bottom in AFM readout, cantspike: size of cantilever spike on wedge, Kcant: measured cantilever stiffness in measurement (without the 0.817 correction for rectangular cantilevers). Distances in um, cantilever stiffness in nN/um.
CondLabel='ParaNitro'; CtrlLabel='Ctrl'; %strings that distinguish treatment condition from control


%-------------



file_s = 'AFMwConfocAna.txt'; %output of analysis parameters, such as cell volume, cell height, radial cortex profile fit etc.
path_s = path_o;
cd(path_o);

list_s = dir('Cell*.lsm');
list_c = struct2cell(list_s);
num_files = size(list_c,2);
FFTarr=zeros(num_files, 100);

file_o=list_c{1,1}; file_no=1;

if contains(file_o,'lsm')
    im_array_0 = imread([path_o, file_o],1);
    im_array = im_array_0(:,:,1);
end

figure(1); hold off;
imagesc(im_array); 
daspect([1 1 1])
hold on

stop = 0;
% write file heads:
WriteFileHead();


for file_num = 1:num_files
    
    try % jump to next video if error occurs
        file_no=file_num;
        file_o = list_c{1,file_num};
        file_name = file_o(1:end-4)
        if length(dir([file_name,'_int_kymograph.fig']))==0
            AnalyzeOne();
        end
    catch ME
        % if error occurs, write simple error message in error file:
        file_errorlog = 'ErrorLog.txt';
        file_id = fopen([path_s file_errorlog],'at');
        fprintf(file_id,'\r\n%s\n',file_name);
        fclose(file_id);
        
        errors = struct2cell(ME.stack);
        num_errors = size(errors,2);
        for n = 1:num_errors
            error_file = errors{2,n};
            error_line = errors{3,n};
            file_id = fopen([path_s file_errorlog],'at');
            fprintf(file_id,'%s, line %g\r\n',error_file,error_line);
            fclose(file_id);
        end
        continue % jump to next video
    end
end
end




% --- Executes on button press in AnalyzeOne.
function AnalyzeOne()
% hObject    handle to AnalyzeOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global path_o path_s file_s num_fitp num_linep len_lines len_lines_um fit_end_time list_s list_c num_files
global stop bleach_frame sigma_gauss boundfit_method fit_delay_time
global num_timescales file_no FFTarr
global offset Kcant cantspike CondLabel CtrlLabel sens NormFac
stop = 0;

handles.image_axes=gcf;
handles.video_slider=struct('Min',1,'Value',1, 'Max',1);
handles.frame_text=struct('String','start');
handles.file_text=struct('String','start');

%figure(1); hold off; figure(2); hold off;
file_num=file_no;

file_o = list_c{1,file_num};
file_name = file_o(1:end-4);

tiffile = dir([file_name(1:end),'.tif']);
%temp=imfinfo(tiffile.name); % possibility to read in infos of the tif image series
% if length(tiffile)==0
%     return;
% end

% get video properties:
[lsminf,~,~] = lsminfo([file_o]);
num_frames = lsminf.TIMESTACKSIZE;
%num_frames=numel(temp); % reads out the frame number in the tif images series
timesteps = lsminf.TimeStamps.TimeSteps;
%lsminf.ScanInfo.DETECTOR_GAIN, lsminf.ScanInfo.POWER{1}
frametime = median(timesteps);             % time per frame
px = lsminf.VoxelSizeX * 10.^6; % µm
len_lines = round(len_lines_um/px);        % length of orthog. lines along cortex in pixels


end_frame = num_frames;                    % calc. until frame...
fnum = 1;

% initialise data arrays:
bounds_all = cell(end_frame,2); fit_all = cell(end_frame,3);
intens_all = zeros(end_frame,num_fitp);
fit_bleach_all = zeros(end_frame,num_fitp);
int_cort_mean_data_all = zeros(end_frame,num_linep); intens_inner_all = zeros(bleach_frame-1,1);% intens_outer_all = zeros(end_frame,1);
int_total=zeros(end_frame,1);

intens_bl_reg_gau = zeros(end_frame-bleach_frame+1,1);
intens_bl_reg_erf = zeros(end_frame-bleach_frame+1,1);
intens_bl_reg_int = zeros(end_frame-bleach_frame+1,1);
intens_bl_reg = zeros(end_frame-bleach_frame+1,1);
intens_bl_reg_radial=zeros(end_frame,num_linep); % used to estimate background intensity in bleached region												  																					 
bl_reg_mean_data=zeros(end_frame-bleach_frame+1,num_linep);
index_rotation = 0;

area_cross_inner_all = zeros(bleach_frame-1,1);area_ring_all = zeros(bleach_frame-1,1);
vol_inner_all = zeros(bleach_frame-1,1);vol_cort_all = zeros(bleach_frame-1,1);
peri_cell_all = zeros(bleach_frame-1,1); area_cell_all = zeros(bleach_frame-1,1); vol_cell_all = zeros(bleach_frame-1,1);

ind_bl_reg_mid = 0; reg_lb_ind = 0; reg_ub_ind = 0; im_save = cell(end_frame);
% Set arrays with data for all frames:
DataAll = {bounds_all,fit_all,intens_all,fit_bleach_all,int_cort_mean_data_all,intens_inner_all, int_total};
% Set image properties:
ImgProp = {path_o,file_o,px,num_frames,fnum,bleach_frame};
% Set discretisation parameters:
DisPara = {num_fitp,num_linep,len_lines,sigma_gauss};
% Set bleach region parameters:
BleachReg = {intens_bl_reg_gau,intens_bl_reg_erf,ind_bl_reg_mid,reg_lb_ind,reg_ub_ind,intens_bl_reg_int,intens_bl_reg,index_rotation,intens_bl_reg_radial};
% Cell dimensions
AreaVols = {area_cross_inner_all,area_ring_all, vol_inner_all,vol_cort_all,peri_cell_all, area_cell_all,vol_cell_all, int_total};


for fnum = 1:end_frame
    if stop == 0
        ImgProp = {path_o,file_o,px,num_frames,fnum,bleach_frame};
        % call calculation function for current frame
        fnum
        [DataAll,BleachReg,AreaVols]= NewFrame(handles,ImgProp,DisPara,boundfit_method,DataAll,BleachReg,AreaVols,sens);
        title(file_name,'Interpreter','none');
        fnum
        if fnum==1
            % save cell image + fitted boundary in tif file:
            save_frame = getframe(handles.image_axes);
            im_save{fnum} = frame2im(save_frame);
            im_file_name = [file_name '_bound.tif'];
            A = im_save{fnum};
            map = colormap(parula);
            imwrite(A,map,im_file_name,'tif');
        end
    end
end
for fnum=1:(bleach_frame-1) % calculate bleached region intensities for intial frames
    ImgProp{5}=fnum;
    [DataAll,BleachReg,AreaVols]= NewFrame(handles,ImgProp,DisPara,boundfit_method,DataAll,BleachReg,AreaVols,sens);        
end
fnum=end_frame; ImgProp{5}=fnum;								
warning('on');
bounds_all = DataAll{1};
fit_all = DataAll{2};
intens_all = DataAll{3};
fit_bleach_all = DataAll{4};
int_cort_mean_data_all = DataAll{5};
intens_inner_all = DataAll{6};
int_total=DataAll{7};


area_cross_inner_all = AreaVols{1};
area_ring_all = AreaVols{2};
vol_inner_all = AreaVols{3};
vol_cort_all = AreaVols{4};
peri_cell_all = AreaVols{5};
area_cell_all = AreaVols{6};
vol_cell_all = AreaVols{7};

% cell areas and volume from time span before bleaching assuming that the cell shape is roughly spherical 
area_cross_inner_mean = mean(area_cross_inner_all(1:min(bleach_frame-1, end_frame))); % time-averaged cross-sectional area of inner cytoplasmic region (up 70% of the radial region)
area_ring_mean = mean(area_ring_all(1:min(bleach_frame-1, end_frame))); %time-averaged area of cytoplasmic rim outside the inner cytoplasmic region
vol_inner_mean = mean(vol_inner_all(1:min(bleach_frame-1, end_frame)));% time-averaged spherical volume of inner cytoplasmic area
vol_cort_mean = mean(vol_cort_all(1:min(bleach_frame-1, end_frame))); % time-averaged spherical cell volume - vol_inner_mean
peri_cell_mean = mean(peri_cell_all(1:min(bleach_frame-1, end_frame)));% time-averaged cell perimeter in 2D images
area_cell_mean = mean(area_cell_all(1:min(bleach_frame-1, end_frame)));% time-averaged total cell surface (3D) assuming a spherical cell shape
vol_cell_mean = mean(vol_cell_all(1:min(bleach_frame-1, end_frame))); % time-averaged cell volume (3D) assuming a spherical cell shape

crosssect_area=area_ring_mean+area_cross_inner_mean;
R2_BC=sqrt(crosssect_area/pi);

%Correction of volumes and area values if cell is in a squished conformation due to AFM confinement
try
    [ConfHeight,TensVal]=GetTension(path_o, Kcant, cantspike, file_name, CondLabel, CtrlLabel, crosssect_area); %TensVal is already corrected by 0.817
    vol_cell_mean=Volume(R2_BC, ConfHeight, ConfHeight/2);
    area_cell_mean=Area(R2_BC, ConfHeight/2, ConfHeight);
    R1_inner=ConfHeight/2-0.5*len_lines_um;R2_inner=R2_BC-0.5*len_lines_um;
    vol_inner_mean=Volume(R2_inner, 2*R1_inner, R1_inner);
    vol_cort_mean=vol_cell_mean-vol_inner_mean;
catch
        ConfHeight=0.99*2*R2_BC; TensVal=0; %no AFM confinement, spherical cell shape anticipated
end


if 1
    try
        int_unbleached=(sum(intens_all(:,BleachReg{5}:num_fitp),2)+sum(intens_all(:,1:BleachReg{4}),2));
        intens_bl_reg_gau = BleachReg{1};
        intens_bl_reg_erf = BleachReg{2};
        ind_bl_reg_mid = BleachReg{3};
        reg_lb_ind = BleachReg{4};
        reg_ub_ind = BleachReg{5};
        intens_bl_reg_int = BleachReg{6};
        intens_bl_reg = BleachReg{7};
        index_rotation = BleachReg{8};
		intens_bl_reg_radial = BleachReg{9};									
        
		%calculation of background in bleach region for time before bleaching
        int_cort = mean(intens_bl_reg_radial(1:bleach_frame-1,:),1);
		BG_bleach_init=mean(int_cort(120:140))*NormFac; %background intensity estimated from radial cortex intensity profile
		%alternative
%        [int_cort_fit,para_cortex,int_cort_fkt, int2_cort_fkt, skewgau,err] = FitCortexIntensity(int_cort);
%        BG_bleach_init=para_cortex(end-1)*NormFac; %background intensity estimated from radial cortex intensity fit
		
		%calculation of background  in bleach region for late time point after bleaching
		int_cort = mean(intens_bl_reg_radial(100:105,:),1);
		BG_bleach_late=mean(int_cort(120:140))*NormFac; %background intensity estimated from radial cortex intensity profile
		%alternative
%        [int_cort_fit,para_cortex,int_cort_fkt, int2_cort_fkt, skewgau,err] = FitCortexIntensity(int_cort);
%        BG_bleach_late=para_cortex(end-1)*NormFac; %background intensity estimated from radial cortex intensity fit
        
		%calculation of averaged cortex background for late time point after bleaching															
		int_cort = mean(int_cort_mean_data_all(100:105,:),1);
		BG_late=mean(int_cort(120:140))*NormFac; %background intensity estimated from radial cortex intensity profile
		%alternative
%        [int_cort_fit,para_cortex,int_cort_fkt, int2_cort_fkt, skewgau,err] = FitCortexIntensity(int_cort);
%        BG_late=para_cortex(end-1)*NormFac; %background intensity estimated from radial cortex intensity fit
		
		%calculation of averaged cortex background for time before bleaching
		int_cort = mean(int_cort_mean_data_all(1:bleach_frame-1,:),1);
		[int_cort_fit,para_cortex,int_cort_fkt, int2_cort_fkt, skewgau,err] = FitCortexIntensity(int_cort);%necessary to keep the correct value of para_cortex
		BG_init=mean(int_cort(120:140))*NormFac; %background intensity estimated from radial cortex intensity profile
		%alternative
%        BG_init=para_cortex(end-1)*NormFac; %background intensity estimated from radial cortex intensity fit
																			
		
        range=max(1,min(bleach_frame+round(5/frametime),(end_frame-50))):end_frame;
        f1=@(a, xdata) a(1)*exp(-a(2)*xdata), 'a','xdata'; a0=[0 0];
        temp1=intens_all(range,1:reg_lb_ind-1); range2=range(temp1(:,1)>0); temp1=temp1(temp1(:,1)>0,:); temp2=intens_all(range2,reg_ub_ind+1:end);
        yvec=mean([temp1 temp2],2)'-(BG_init-(BG_init-BG_late)/102*range2);
        [a, resnorm,residual,exitflag,output,lambda,jacobian]=lsqcurvefit(f1,a0,range2, yvec);
        bleachrate=a(2);
        bleachrate2=-2*(median(int_total(end-10:end))-median(int_total(1:(bleach_frame-1))))/(median(int_total(end-10:end))+median(int_total(1:(bleach_frame-1))))/(end_frame-bleach_frame);%alternative bleach rate estimation
        rsquared=1-(sum((f1(a,range2)-yvec).^2)./sum((yvec-mean(yvec)).^2));
        
        % plot and save intensity kymograph:
        intens_fig = figure('Name',['Intensity kymograph ',file_name]);
        PlotIntensities(intens_all-(ones(end_frame,num_fitp).*(BG_init-(BG_init-BG_late)/100.*(1:end_frame))'),intens_fig,1, bleachrate);
        savefig([file_name,'_int_kymograph.fig']);
        close(intens_fig);
        
        intens_bl_reg_gau_short = ...
            intens_bl_reg_gau(1:end_frame-bleach_frame+1);
        intens_bl_reg_erf_short = ...
            intens_bl_reg_erf(1:end_frame-bleach_frame+1);
        intens_bl_reg_tot_short = ...
            intens_bl_reg(1:end_frame-bleach_frame+1);
        
        
        %The intensity function used below should be equal to 0 when the cortex is totally dark, and to 1 when the (bleaching corrected) cortex is fully recovered:
        recovery_figure = figure('name',['Recovery fit ',file_name]);
        frameRange = (1:end_frame-bleach_frame+1)';
        int=(intens_bl_reg_tot_short-smooth(mean(intens_bl_reg_radial(bleach_frame:end_frame,120:140),2),20)*NormFac)/(mean(mean(intens_all(1:(bleach_frame-1),reg_lb_ind:reg_ub_ind)))-BG_bleach_init); %using a time-variable background intensity estimate
        plt_corr=plot(frameRange*frametime,int.*exp(frameRange*bleachrate),'ro'); hold on;
        plt_uncorr=plot(frameRange*frametime,int,'r.'); hold on;xlabel('t in [s]'); ylabel('intensity');
		num_timescales=1;
        [FitOutputExp]=FitTimeScale2(num_timescales, frametime, end_frame, int, file_name, bleachrate);
        ex = @(p,t)-p(2)*exp(-t/p(1))+p(3);
        plt_fit=plot(frameRange*frametime,ex(FitOutputExp(1:3),frameRange*frametime),'r','LineWidth',1);
        chH = get(gca,'Children');set(gca,'Children',[chH(2:end);chH(1)]);clear chH;%change the order of the children of the axis for subsequent processing of the figure by AnalysisScriptSummary
        title(['I(t) plot of bleach region, ',file_name], 'Interpreter','none')
        legend([plt_corr,plt_uncorr,plt_fit],'cortex intensity, bleaching corrected','cortex intensity, no correction',['exponential fit: $\tau=' num2str(FitOutputExp(1)) 's$'], 'Interpreter', 'latex');
        savefig([file_name,'_recovery.fig']);
        close(recovery_figure);
        
        int_cort_fit_cyto = int_cort_fit(1); % value of the fitted intensity profile at the innermost point
        intens_inner_all = intens_inner_all(1:bleach_frame-1);
        intens_inner_mean = mean(intens_inner_all);
        
        % Cortex properties from time span before bleaching
        s=linspace(0,1,num_linep);
        integr_gau = trapz(s,skewgau(para_cortex,s))*px*len_lines;
        cortex_2D = peri_cell_mean * integr_gau; % crosssection
        cortex_3D = area_cell_mean * integr_gau; % 3D cell
        
        % Cytosol intensities from time span before bleaching
        % crosssection
        cyto_inner_2D = area_cross_inner_mean * intens_inner_mean;
        cyto_ring_2D = area_ring_mean * int_cort_fit_cyto;
        cytosol_2D = cyto_inner_2D + cyto_ring_2D;
        
        % 3D cell from time span before bleaching
        cyto_inner_3D = vol_inner_mean * (intens_inner_mean-BG_init/NormFac);
        cyto_ring_3D = vol_cort_mean * int_cort_fit_cyto;
        cytosol_3D = cyto_inner_3D + cyto_ring_3D;
        
        cortcyt_2D = cortex_2D./cytosol_2D;
        cortcyt_3D = cortex_3D./cytosol_3D;
        
        intens_dist_fig = figure('name',['Cell cortex ',file_name]);
        
        plot(s*len_lines*px,int_cort); hold on
        plot(s*len_lines*px,int2_cort_fkt(para_cortex,s));
        legend('mean data','Fit');
        title(['Averaged radial Intensity profile of cell cortex, mean for frames 1-',...
            num2str(bleach_frame-1),' and whole cell perimeter']);
        xlabel('radial coordinate [µm]'); ylabel('Intensity');
        savefig([file_name,'_cell_cortex.fig']);
        close(intens_dist_fig);
    catch
        cortex_3D=0;cytosol_3D=0; cortcyt_3D=0; para_cortex=[0 0 0 0 0 0]; rsquared=0; bleachrate=1; bleachrate2=1;FitOutputExp=[0 0 0 0 0 0 0];
    end
     
    % Save results in text file:
    file_s_id = fopen([path_s file_s],'a');    
    fprintf(file_s_id,'%s \t',file_name);
    fprintf(file_s_id,'%g \t',crosssect_area, vol_cell_mean,cortex_3D,cytosol_3D,cortcyt_3D,para_cortex,...
            1/bleachrate,1/bleachrate2, lsminf.TimeStamps.Stamps(1), TensVal, ConfHeight,...
			FitOutputExp([1 4 2 5 3 6 7]), rsquared, sens);
    fprintf(file_s_id,'\n');
    fclose(file_s_id);
end

clearvars
1

    function Vol=Volume(R2,Z, R1)
        if Z>(2*R2)
            Z=2*R2; R1=R2;
        end
        Vol=pi*(Z.*(2*R1.^2 -2*R1.*R2 + R2.^2-Z.^2/12)+(2*asin(0.5*Z./R1)+sin(2*asin(0.5*Z./R1))).*(R2 - R1).*R1.^2);
    end

    function A=Area(R2, R1, Z)
        A=pi*(4*R1.*(R2 - R1).*(asin(0.5*Z./R1)+cos(asin(0.5*Z./R1))-1)+2*R1.*Z+2*R2.^2-Z.^2/2);
    end
end

